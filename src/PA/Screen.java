/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package PA;

import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;
import java.awt.CardLayout;
import java.awt.HeadlessException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author FANSKY
 */
public class Screen extends javax.swing.JFrame {

    private boolean isAdmin = false, isCari = false, isDown = false;
    private int i = 3, j = 3;
    private boolean isAlive = false;
    private String temp_tgl, temp_bln, temp_thn;
    private String id_fak = null;
    private String nip = null, temp = null, id = null;
    private boolean ischeck = false, isi = false, isi1 = false, isi2 = false, isi3 = false, isi4 = false, isi5 = false, isi6 = false, isi7 = false, isi8 = false;
    private String URL="";
    private String URLAsal ="";
    private boolean ada = false;
    public Screen() {

        String[] name = {"Nama Mahasiswa", "NIM", "Judul PA", "Nama Pembimbing", "Fakultas"};
        DefaultTableModel tb = new DefaultTableModel(name, 0) {
            @Override
            public boolean isCellEditable(int row, int col) {
                return false;
            }
        };

        initComponents();
        
        jTable2.setModel(tb);
        CardLayout cl = (CardLayout) Body.getLayout();
        cl.show(Body, "pilih_user");
        jLabel8.setVisible(false);
        jLabel9.setVisible(false);
        nim1.setEditable(false);
        
        jTextField16.setEditable(false);
        jTextField17.setEditable(false);
        jTextField15.setEditable(false);
        jTextArea1.setEditable(false);
        jTextField19.setEditable(false);
        jTextField20.setEditable(false);
        jTextField18.setEditable(false);
        jTextArea3.setEditable(false);




    }

    public void update(String no, int i, JTable tabel) {
        String[] nameColoum = {no, "Nama Mahasiswa", "NIM", "Judul PA", "Nama Pembimbing", "Fakultas"};
        DefaultTableModel tb = new DefaultTableModel(nameColoum, 0) {
            @Override
            public boolean isCellEditable(int row, int col) {
                return false;
            }
        };

        Database db = new Database();
        ResultSet rs = null;
        i = 1;
        try {

            rs = db.getResult("select nama_mahasiswa,nim,judul_pa,nama_pembimbing,nama_fakultas from mahasiswa join pa using (nim) join pembimbing using(nip) join fakultas using (id_fakultas) order by id_pa desc");
            while (rs.next()) {
                String[] data = {"" + i, rs.getString("nama_mahasiswa"), rs.getString("nim"), rs.getString("judul_pa"), rs.getString("nama_pembimbing"), rs.getString("nama_fakultas")};
                tb.addRow(data);

                i++;
            }
            rs.close();
        } catch (SQLException f) {
            JOptionPane.showMessageDialog(null, "" + f.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }

        tabel.setModel(tb);


    }

    public void update(String no, JTable tabel) {
        String[] nameColoum = {no, "Nama Mahasiswa", "NIM", "Judul PA", "Nama Pembimbing", "Fakultas"};
        DefaultTableModel tb = new DefaultTableModel(nameColoum, 0) {
            @Override
            public boolean isCellEditable(int row, int col) {
                return false;
            }
        };

        Database db = new Database();
        ResultSet rs = null;

        try {

            rs = db.getResult("select nama_mahasiswa,nim,judul_pa,nama_pembimbing, id_pa,nama_fakultas from mahasiswa join pa using (nim) join pembimbing using(nip) join fakultas using(id_fakultas)");
            while (rs.next()) {
                String[] data = {rs.getString("id_pa"), rs.getString("nama_mahasiswa"), rs.getString("nim"), rs.getString("judul_pa"), rs.getString("nama_pembimbing"), rs.getString("nama_fakultas")};
                tb.addRow(data);


            }
            rs.close();
        } catch (SQLException f) {
            JOptionPane.showMessageDialog(null, "" + f.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }

        tabel.setModel(tb);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        back = new javax.swing.JPanel();
        utama = new javax.swing.JPanel();
        home = new javax.swing.JPanel();
        header = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        Body = new javax.swing.JPanel();
        body_depan = new javax.swing.JPanel();
        kanan = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        kiri = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel55 = new javax.swing.JLabel();
        jLabel56 = new javax.swing.JLabel();
        Login = new javax.swing.JPanel();
        awal = new javax.swing.JPanel();
        body_login = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jPasswordField1 = new javax.swing.JPasswordField();
        jLabel19 = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        jLabel25 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jButton8 = new javax.swing.JButton();
        Menu_mhs = new javax.swing.JPanel();
        header_mhs = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jButton21 = new javax.swing.JButton();
        body_mhs = new javax.swing.JPanel();
        menu_utama = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jButton19 = new javax.swing.JButton();
        jButton20 = new javax.swing.JButton();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        Menu_Upload = new javax.swing.JPanel();
        body_upload = new javax.swing.JPanel();
        jButton11 = new javax.swing.JButton();
        jLabel32 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextArea2 = new javax.swing.JTextArea();
        jCheckBox2 = new javax.swing.JCheckBox();
        jLabel40 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        jButton33 = new javax.swing.JButton();
        jTextField21 = new javax.swing.JTextField();
        jLabel51 = new javax.swing.JLabel();
        jLabel107 = new javax.swing.JLabel();
        jButton22 = new javax.swing.JButton();
        menu_edit_akun = new javax.swing.JPanel();
        body_edit = new javax.swing.JPanel();
        page3 = new javax.swing.JPanel();
        jLabel57 = new javax.swing.JLabel();
        jLabel58 = new javax.swing.JLabel();
        nama1 = new javax.swing.JTextField();
        nim1 = new javax.swing.JTextField();
        jLabel59 = new javax.swing.JLabel();
        no_hp1 = new javax.swing.JTextField();
        no_telp1 = new javax.swing.JTextField();
        jLabel60 = new javax.swing.JLabel();
        jLabel61 = new javax.swing.JLabel();
        email1 = new javax.swing.JTextField();
        jScrollPane5 = new javax.swing.JScrollPane();
        alamat1 = new javax.swing.JTextArea();
        jButton23 = new javax.swing.JButton();
        jLabel98 = new javax.swing.JLabel();
        jLabel99 = new javax.swing.JLabel();
        page4 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel62 = new javax.swing.JLabel();
        jLabel63 = new javax.swing.JLabel();
        jLabel64 = new javax.swing.JLabel();
        password1 = new javax.swing.JPasswordField();
        ulang_password1 = new javax.swing.JPasswordField();
        fakultas1 = new javax.swing.JComboBox();
        jLabel65 = new javax.swing.JLabel();
        jButton24 = new javax.swing.JButton();
        jButton25 = new javax.swing.JButton();
        jLabel67 = new javax.swing.JLabel();
        tahun1 = new javax.swing.JComboBox();
        jLabel68 = new javax.swing.JLabel();
        pembimbing1 = new javax.swing.JComboBox();
        jLabel100 = new javax.swing.JLabel();
        jDateChooser2 = new com.toedter.calendar.JDateChooser();
        jYearChooser1 = new com.toedter.calendar.JYearChooser();
        jButton29 = new javax.swing.JButton();
        registrasi = new javax.swing.JPanel();
        title_registrasi = new javax.swing.JPanel();
        jLabel23 = new javax.swing.JLabel();
        body_registrasi = new javax.swing.JPanel();
        page1 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        nama = new javax.swing.JTextField();
        nim = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        no_hp = new javax.swing.JTextField();
        no_telp = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        email = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        alamat = new javax.swing.JTextArea();
        jButton5 = new javax.swing.JButton();
        jLabel43 = new javax.swing.JLabel();
        jLabel66 = new javax.swing.JLabel();
        jLabel93 = new javax.swing.JLabel();
        jLabel95 = new javax.swing.JLabel();
        jLabel96 = new javax.swing.JLabel();
        page2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        password = new javax.swing.JPasswordField();
        ulang_password = new javax.swing.JPasswordField();
        fakultas = new javax.swing.JComboBox();
        jLabel29 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jCheckBox1 = new javax.swing.JCheckBox();
        jButton6 = new javax.swing.JButton();
        jLabel44 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        tahun = new javax.swing.JComboBox();
        jLabel46 = new javax.swing.JLabel();
        pembimbing = new javax.swing.JComboBox();
        jLabel84 = new javax.swing.JLabel();
        jLabel94 = new javax.swing.JLabel();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jButton9 = new javax.swing.JButton();
        lupa_pass = new javax.swing.JPanel();
        title_lupa = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        body_lupa = new javax.swing.JPanel();
        jLabel31 = new javax.swing.JLabel();
        jTextField8 = new javax.swing.JTextField();
        jButton7 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        Menu_cari = new javax.swing.JPanel();
        title_cari = new javax.swing.JPanel();
        jLabel33 = new javax.swing.JLabel();
        body_cari = new javax.swing.JPanel();
        cari_kiri = new javax.swing.JPanel();
        jComboBox6 = new javax.swing.JComboBox();
        jLabel35 = new javax.swing.JLabel();
        jTextField9 = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        jButton12 = new javax.swing.JButton();
        cari_kanan = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jComboBox5 = new javax.swing.JComboBox();
        jLabel34 = new javax.swing.JLabel();
        jButton13 = new javax.swing.JButton();
        pilih_user = new javax.swing.JPanel();
        body_pilih = new javax.swing.JPanel();
        jButton14 = new javax.swing.JButton();
        jButton15 = new javax.swing.JButton();
        jLabel53 = new javax.swing.JLabel();
        jLabel54 = new javax.swing.JLabel();
        title_pilih = new javax.swing.JPanel();
        jLabel37 = new javax.swing.JLabel();
        Admin = new javax.swing.JPanel();
        body_admin = new javax.swing.JPanel();
        title_login = new javax.swing.JPanel();
        jLabel42 = new javax.swing.JLabel();
        login_admin = new javax.swing.JPanel();
        jLabel38 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jTextField10 = new javax.swing.JTextField();
        jPasswordField4 = new javax.swing.JPasswordField();
        jButton16 = new javax.swing.JButton();
        jLabel41 = new javax.swing.JLabel();
        jButton17 = new javax.swing.JButton();
        masuk_admin = new javax.swing.JPanel();
        title_masuk = new javax.swing.JPanel();
        jLabel45 = new javax.swing.JLabel();
        jButton18 = new javax.swing.JButton();
        body_masuk = new javax.swing.JPanel();
        menu_utama_admin = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jButton27 = new javax.swing.JButton();
        jButton34 = new javax.swing.JButton();
        jLabel70 = new javax.swing.JLabel();
        jLabel71 = new javax.swing.JLabel();
        jButton28 = new javax.swing.JButton();
        jLabel82 = new javax.swing.JLabel();
        input_data = new javax.swing.JPanel();
        header_input = new javax.swing.JPanel();
        jLabel52 = new javax.swing.JLabel();
        body_input = new javax.swing.JPanel();
        input_dosen = new javax.swing.JPanel();
        jLabel73 = new javax.swing.JLabel();
        jLabel74 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jButton38 = new javax.swing.JButton();
        footer_input = new javax.swing.JPanel();
        jButton42 = new javax.swing.JButton();
        edit_data = new javax.swing.JPanel();
        header_edit = new javax.swing.JPanel();
        jLabel81 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox();
        jLabel83 = new javax.swing.JLabel();
        jTextField13 = new javax.swing.JTextField();
        jButton26 = new javax.swing.JButton();
        footer_edit = new javax.swing.JPanel();
        jButton43 = new javax.swing.JButton();
        body_edit_data = new javax.swing.JPanel();
        body_edit_data_kiri = new javax.swing.JPanel();
        edit_pa = new javax.swing.JPanel();
        jLabel77 = new javax.swing.JLabel();
        jLabel78 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();
        jTextField7 = new javax.swing.JTextField();
        jButton40 = new javax.swing.JButton();
        edit_dosen = new javax.swing.JPanel();
        jLabel79 = new javax.swing.JLabel();
        jLabel80 = new javax.swing.JLabel();
        jTextField11 = new javax.swing.JTextField();
        jTextField12 = new javax.swing.JTextField();
        jButton41 = new javax.swing.JButton();
        body_edit_data_kanan = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        jTable4 = new javax.swing.JTable();
        download_data = new javax.swing.JPanel();
        header_download = new javax.swing.JPanel();
        jLabel69 = new javax.swing.JLabel();
        body_download = new javax.swing.JPanel();
        kiri_body = new javax.swing.JPanel();
        jLabel85 = new javax.swing.JLabel();
        jTextField14 = new javax.swing.JTextField();
        jButton31 = new javax.swing.JButton();
        kanan_body = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jLabel86 = new javax.swing.JLabel();
        jComboBox3 = new javax.swing.JComboBox();
        jButton30 = new javax.swing.JButton();
        display_detail1 = new javax.swing.JPanel();
        header_detail1 = new javax.swing.JPanel();
        jLabel101 = new javax.swing.JLabel();
        body_detail1 = new javax.swing.JPanel();
        jLabel102 = new javax.swing.JLabel();
        jLabel103 = new javax.swing.JLabel();
        jLabel104 = new javax.swing.JLabel();
        jTextField18 = new javax.swing.JTextField();
        jTextField19 = new javax.swing.JTextField();
        jTextField20 = new javax.swing.JTextField();
        jLabel105 = new javax.swing.JLabel();
        jScrollPane9 = new javax.swing.JScrollPane();
        jTextArea3 = new javax.swing.JTextArea();
        jPanel7 = new javax.swing.JPanel();
        jButton36 = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        jLabel106 = new javax.swing.JLabel();
        footer_detail1 = new javax.swing.JPanel();
        jButton37 = new javax.swing.JButton();
        display_detail = new javax.swing.JPanel();
        header_detail = new javax.swing.JPanel();
        jLabel87 = new javax.swing.JLabel();
        body_detail = new javax.swing.JPanel();
        jLabel88 = new javax.swing.JLabel();
        jLabel89 = new javax.swing.JLabel();
        jLabel90 = new javax.swing.JLabel();
        jTextField15 = new javax.swing.JTextField();
        jTextField16 = new javax.swing.JTextField();
        jTextField17 = new javax.swing.JTextField();
        jLabel92 = new javax.swing.JLabel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jPanel6 = new javax.swing.JPanel();
        jLabel91 = new javax.swing.JLabel();
        footer_detail = new javax.swing.JPanel();
        jButton32 = new javax.swing.JButton();
        footer = new javax.swing.JLabel();
        background = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        file = new javax.swing.JMenu();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        edit = new javax.swing.JMenu();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        help = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        About = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Aplikasi Pengelolaan Proyek Akhir");
        setPreferredSize(new java.awt.Dimension(1364, 760));
        setResizable(false);

        back.setBackground(new java.awt.Color(255, 255, 255));
        back.setPreferredSize(new java.awt.Dimension(1365, 770));

        home.setBackground(new java.awt.Color(255, 255, 255));
        home.setPreferredSize(new java.awt.Dimension(1000, 792));

        header.setBackground(new java.awt.Color(255, 255, 255));

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/ITT.png"))); // NOI18N

        jLabel16.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(0, 153, 204));
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/sasa.png"))); // NOI18N
        jLabel16.setText("Aplikasi Pengelolaan Proyek Akhir");
        jLabel16.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));

        jLabel9.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(0, 153, 204));
        jLabel9.setText("Proyek Akhir Terbaru");

        jLabel8.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 153, 204));
        jLabel8.setText("Menu Utama");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 203, Short.MAX_VALUE)
                .addComponent(jLabel9)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel9)
                .addComponent(jLabel8))
        );

        javax.swing.GroupLayout headerLayout = new javax.swing.GroupLayout(header);
        header.setLayout(headerLayout);
        headerLayout.setHorizontalGroup(
            headerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(headerLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(headerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(headerLayout.createSequentialGroup()
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(headerLayout.createSequentialGroup()
                        .addComponent(jLabel16)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 481, Short.MAX_VALUE)
                        .addComponent(jLabel10)
                        .addGap(99, 99, 99))))
        );
        headerLayout.setVerticalGroup(
            headerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, headerLayout.createSequentialGroup()
                .addContainerGap(17, Short.MAX_VALUE)
                .addGroup(headerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addGap(51, 51, 51)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        Body.setBackground(new java.awt.Color(255, 255, 255));
        Body.setPreferredSize(new java.awt.Dimension(1100, 400));
        Body.setLayout(new java.awt.CardLayout());

        body_depan.setBackground(new java.awt.Color(255, 255, 255));
        body_depan.setPreferredSize(new java.awt.Dimension(1209, 500));

        kanan.setBackground(new java.awt.Color(255, 255, 255));

        jScrollPane1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        jTable1.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "NO", "Nama", "NIM", "Judul", "Pembimbing"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setVerifyInputWhenFocusTarget(false);
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        jTable1.getColumnModel().getColumn(0).setPreferredWidth(30);

        javax.swing.GroupLayout kananLayout = new javax.swing.GroupLayout(kanan);
        kanan.setLayout(kananLayout);
        kananLayout.setHorizontalGroup(
            kananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 744, Short.MAX_VALUE)
        );
        kananLayout.setVerticalGroup(
            kananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );

        kiri.setBackground(new java.awt.Color(255, 255, 255));
        kiri.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));
        kiri.setPreferredSize(new java.awt.Dimension(300, 333));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/login.png"))); // NOI18N
        jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Search2.png"))); // NOI18N
        jButton2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton2.setMaximumSize(new java.awt.Dimension(77, 53));
        jButton2.setMinimumSize(new java.awt.Dimension(77, 53));
        jButton2.setPreferredSize(new java.awt.Dimension(77, 53));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jLabel1.setText("Setelah masuk, anda dapat");

        jLabel2.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jLabel2.setText("Cari File PA berdasarkan");

        jLabel3.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jLabel3.setText("NIM, Judul PA, Fakultas");

        jLabel4.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jLabel4.setText("mengedit profile anda atau");

        jLabel5.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jLabel5.setText("meng-upload file PA anda.");

        jLabel11.setFont(new java.awt.Font("Calibri", 3, 18)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(0, 153, 204));
        jLabel11.setText("Login");

        jLabel12.setFont(new java.awt.Font("Calibri", 3, 18)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(0, 153, 204));
        jLabel12.setText("Cari Proyek Akhir");

        jLabel55.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jLabel55.setText("Jika anda belum memiliki akun, ");

        jLabel56.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jLabel56.setText("Silahkan daftar terlebih dahulu !");

        javax.swing.GroupLayout kiriLayout = new javax.swing.GroupLayout(kiri);
        kiri.setLayout(kiriLayout);
        kiriLayout.setHorizontalGroup(
            kiriLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, kiriLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(kiriLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(kiriLayout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(kiriLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(kiriLayout.createSequentialGroup()
                                .addGroup(kiriLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel55)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel56)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(kiriLayout.createSequentialGroup()
                        .addGroup(kiriLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(kiriLayout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jLabel11))
                            .addComponent(jLabel12)
                            .addGroup(kiriLayout.createSequentialGroup()
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(kiriLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        kiriLayout.setVerticalGroup(
            kiriLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(kiriLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(kiriLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(kiriLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel55)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel56)))
                .addGap(46, 46, 46)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(kiriLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(kiriLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3))
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(51, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout body_depanLayout = new javax.swing.GroupLayout(body_depan);
        body_depan.setLayout(body_depanLayout);
        body_depanLayout.setHorizontalGroup(
            body_depanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(body_depanLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(kiri, javax.swing.GroupLayout.PREFERRED_SIZE, 306, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34)
                .addComponent(kanan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        body_depanLayout.setVerticalGroup(
            body_depanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(body_depanLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(body_depanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(kanan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(kiri, javax.swing.GroupLayout.DEFAULT_SIZE, 383, Short.MAX_VALUE))
                .addGap(0, 7, Short.MAX_VALUE))
        );

        Body.add(body_depan, "depan");

        Login.setBackground(new java.awt.Color(255, 255, 255));
        Login.setLayout(new java.awt.CardLayout());

        awal.setBackground(new java.awt.Color(255, 255, 255));
        awal.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        body_login.setBackground(new java.awt.Color(0, 153, 204));
        body_login.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        jLabel7.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("NIM");

        jLabel14.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Password");

        jTextField1.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });

        jPasswordField1.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jPasswordField1.setToolTipText("Pastikan Capslock hidup atau tidak");

        jLabel19.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setText("Daftar sini !");
        jLabel19.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel19.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel19MouseClicked(evt);
            }
        });

        jButton4.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jButton4.setText("Masuk");
        jButton4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel25.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(255, 255, 255));
        jLabel25.setText("Bantuan Login");
        jLabel25.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel25.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel25MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout body_loginLayout = new javax.swing.GroupLayout(body_login);
        body_login.setLayout(body_loginLayout);
        body_loginLayout.setHorizontalGroup(
            body_loginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(body_loginLayout.createSequentialGroup()
                .addGap(211, 211, 211)
                .addGroup(body_loginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel19)
                    .addGroup(body_loginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jButton4)
                        .addGroup(body_loginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(body_loginLayout.createSequentialGroup()
                                .addComponent(jLabel14)
                                .addGap(39, 39, 39)
                                .addComponent(jPasswordField1, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(body_loginLayout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(jLabel25))
                .addContainerGap(233, Short.MAX_VALUE))
        );
        body_loginLayout.setVerticalGroup(
            body_loginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, body_loginLayout.createSequentialGroup()
                .addContainerGap(51, Short.MAX_VALUE)
                .addGroup(body_loginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(body_loginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jPasswordField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton4)
                .addGap(16, 16, 16)
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel25)
                .addGap(68, 68, 68))
        );

        jLabel24.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(0, 153, 204));
        jLabel24.setText("Form Login");

        jButton8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/go-back-icon.png"))); // NOI18N
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout awalLayout = new javax.swing.GroupLayout(awal);
        awal.setLayout(awalLayout);
        awalLayout.setHorizontalGroup(
            awalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, awalLayout.createSequentialGroup()
                .addContainerGap(191, Short.MAX_VALUE)
                .addGroup(awalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(body_login, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(194, 194, 194))
            .addGroup(awalLayout.createSequentialGroup()
                .addGap(216, 216, 216)
                .addComponent(jLabel24)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        awalLayout.setVerticalGroup(
            awalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(awalLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(body_login, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton8)
                .addContainerGap(24, Short.MAX_VALUE))
        );

        Login.add(awal, "awal");

        Menu_mhs.setBackground(new java.awt.Color(255, 255, 255));
        Menu_mhs.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        header_mhs.setBackground(new java.awt.Color(255, 255, 255));

        jLabel6.setFont(new java.awt.Font("Calibri", 3, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 153, 204));
        jLabel6.setText("?");

        jButton21.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jButton21.setText("Log Out");
        jButton21.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton21ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout header_mhsLayout = new javax.swing.GroupLayout(header_mhs);
        header_mhs.setLayout(header_mhsLayout);
        header_mhsLayout.setHorizontalGroup(
            header_mhsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(header_mhsLayout.createSequentialGroup()
                .addContainerGap(520, Short.MAX_VALUE)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButton21, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(110, 110, 110))
        );
        header_mhsLayout.setVerticalGroup(
            header_mhsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(header_mhsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(header_mhsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton21, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
                    .addGroup(header_mhsLayout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );

        body_mhs.setBackground(new java.awt.Color(255, 255, 255));
        body_mhs.setLayout(new java.awt.CardLayout());

        menu_utama.setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(0, 153, 204));
        jPanel1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        jButton19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/upload.png"))); // NOI18N
        jButton19.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton19ActionPerformed(evt);
            }
        });

        jButton20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/edit_user.png"))); // NOI18N
        jButton20.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton20ActionPerformed(evt);
            }
        });

        jLabel48.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel48.setForeground(new java.awt.Color(255, 255, 255));
        jLabel48.setText("Upload File PA");

        jLabel49.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel49.setForeground(new java.awt.Color(255, 255, 255));
        jLabel49.setText("Edit Akun");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(100, 100, 100)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton19, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addComponent(jLabel48)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 191, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jButton20, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(95, 95, 95))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel49)
                        .addGap(147, 147, 147))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel48)
                    .addComponent(jLabel49))
                .addContainerGap(28, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout menu_utamaLayout = new javax.swing.GroupLayout(menu_utama);
        menu_utama.setLayout(menu_utamaLayout);
        menu_utamaLayout.setHorizontalGroup(
            menu_utamaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(menu_utamaLayout.createSequentialGroup()
                .addGap(143, 143, 143)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(144, Short.MAX_VALUE))
        );
        menu_utamaLayout.setVerticalGroup(
            menu_utamaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(menu_utamaLayout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(85, Short.MAX_VALUE))
        );

        body_mhs.add(menu_utama, "menu_utama");

        Menu_Upload.setBackground(new java.awt.Color(255, 255, 255));
        Menu_Upload.setPreferredSize(new java.awt.Dimension(1100, 400));

        body_upload.setBackground(new java.awt.Color(255, 255, 255));
        body_upload.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        jButton11.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jButton11.setText("Pilih File");
        jButton11.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton11.setEnabled(false);
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        jLabel32.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel32.setText("Abstraksi *      :");

        jTextArea2.setColumns(20);
        jTextArea2.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jTextArea2.setLineWrap(true);
        jTextArea2.setRows(5);
        jTextArea2.setAutoscrolls(false);
        jTextArea2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextArea2KeyTyped(evt);
            }
        });
        jScrollPane3.setViewportView(jTextArea2);

        jCheckBox2.setBackground(new java.awt.Color(255, 255, 255));
        jCheckBox2.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jCheckBox2.setText("Saya bersedia menerima resiko apapun jika saya tidak mematuhi peraturan yang ada");
        jCheckBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox2ActionPerformed(evt);
            }
        });

        jLabel40.setFont(new java.awt.Font("Calibri", 2, 12)); // NOI18N
        jLabel40.setText("Note : Untuk file upload harus berformat *.pdf");

        jLabel47.setBackground(new java.awt.Color(255, 255, 255));
        jLabel47.setFont(new java.awt.Font("Calibri", 2, 12)); // NOI18N
        jLabel47.setText("* : wajib di isi");

        jLabel50.setBackground(new java.awt.Color(255, 255, 255));
        jLabel50.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel50.setText("Judul PA anda :");

        jButton33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/check.png"))); // NOI18N
        jButton33.setEnabled(false);
        jButton33.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton33ActionPerformed(evt);
            }
        });

        jTextField21.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jTextField21.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextField21KeyTyped(evt);
            }
        });

        jLabel51.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel51.setText("Nama File       :");

        jLabel107.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel107.setText("?");

        javax.swing.GroupLayout body_uploadLayout = new javax.swing.GroupLayout(body_upload);
        body_upload.setLayout(body_uploadLayout);
        body_uploadLayout.setHorizontalGroup(
            body_uploadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(body_uploadLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(body_uploadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(body_uploadLayout.createSequentialGroup()
                        .addGroup(body_uploadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jCheckBox2)
                            .addGroup(body_uploadLayout.createSequentialGroup()
                                .addGroup(body_uploadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(body_uploadLayout.createSequentialGroup()
                                        .addComponent(jLabel50)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jTextField21))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, body_uploadLayout.createSequentialGroup()
                                        .addComponent(jLabel32)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 283, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButton11)))
                        .addContainerGap(161, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, body_uploadLayout.createSequentialGroup()
                        .addGroup(body_uploadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(body_uploadLayout.createSequentialGroup()
                                .addComponent(jLabel51, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel107, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, body_uploadLayout.createSequentialGroup()
                                .addGroup(body_uploadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel40)
                                    .addGroup(body_uploadLayout.createSequentialGroup()
                                        .addGap(36, 36, 36)
                                        .addComponent(jLabel47)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton33)))
                        .addGap(26, 26, 26))))
        );
        body_uploadLayout.setVerticalGroup(
            body_uploadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(body_uploadLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jLabel40)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(body_uploadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel50)
                    .addComponent(jTextField21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(body_uploadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel32)
                    .addComponent(jButton11, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(body_uploadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel51)
                    .addComponent(jLabel107))
                .addGap(18, 18, 18)
                .addComponent(jCheckBox2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(body_uploadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton33, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel47, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jButton22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/go-back-icon.png"))); // NOI18N
        jButton22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton22ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout Menu_UploadLayout = new javax.swing.GroupLayout(Menu_Upload);
        Menu_Upload.setLayout(Menu_UploadLayout);
        Menu_UploadLayout.setHorizontalGroup(
            Menu_UploadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Menu_UploadLayout.createSequentialGroup()
                .addGap(168, 168, 168)
                .addGroup(Menu_UploadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton22, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(body_upload, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(172, Short.MAX_VALUE))
        );
        Menu_UploadLayout.setVerticalGroup(
            Menu_UploadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Menu_UploadLayout.createSequentialGroup()
                .addComponent(body_upload, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton22, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        body_mhs.add(Menu_Upload, "upload");

        menu_edit_akun.setBackground(new java.awt.Color(255, 255, 255));

        body_edit.setBackground(new java.awt.Color(255, 255, 255));
        body_edit.setLayout(new java.awt.CardLayout());

        page3.setBackground(new java.awt.Color(255, 255, 255));
        page3.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(51, 153, 255), 1, true));

        jLabel57.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel57.setText("Nama  :");

        jLabel58.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel58.setText("NIM  :");

        nama1.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        nama1.setToolTipText("");

        nim1.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        nim1.setToolTipText("NIM");

        jLabel59.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel59.setText("Nomor Telepon  :");

        no_hp1.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        no_hp1.setToolTipText("Nomor HP");

        no_telp1.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        no_telp1.setToolTipText("Nomor telp");

        jLabel60.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel60.setText("Email  :");

        jLabel61.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel61.setText("Alamat  :");

        email1.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        email1.setToolTipText("");

        alamat1.setColumns(20);
        alamat1.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        alamat1.setLineWrap(true);
        alamat1.setRows(5);
        jScrollPane5.setViewportView(alamat1);

        jButton23.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jButton23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/go-next-icon.png"))); // NOI18N
        jButton23.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton23ActionPerformed(evt);
            }
        });

        jLabel98.setFont(new java.awt.Font("Calibri", 2, 12)); // NOI18N
        jLabel98.setText("No HP");

        jLabel99.setFont(new java.awt.Font("Calibri", 2, 12)); // NOI18N
        jLabel99.setText("No telepon");

        javax.swing.GroupLayout page3Layout = new javax.swing.GroupLayout(page3);
        page3.setLayout(page3Layout);
        page3Layout.setHorizontalGroup(
            page3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, page3Layout.createSequentialGroup()
                .addContainerGap(29, Short.MAX_VALUE)
                .addGroup(page3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel57)
                    .addComponent(jLabel59)
                    .addComponent(jLabel60)
                    .addComponent(jLabel61)
                    .addComponent(jLabel58))
                .addGap(42, 42, 42)
                .addGroup(page3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane5)
                    .addComponent(nama1)
                    .addComponent(email1, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, page3Layout.createSequentialGroup()
                        .addGroup(page3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(page3Layout.createSequentialGroup()
                                .addComponent(no_hp1)
                                .addGap(18, 18, 18))
                            .addGroup(page3Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jLabel98)
                                .addGap(112, 112, 112)))
                        .addGroup(page3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(page3Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jLabel99))
                            .addComponent(no_telp1, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(nim1, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(98, 98, 98)
                .addComponent(jButton23, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        page3Layout.setVerticalGroup(
            page3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(page3Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(page3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel57)
                    .addComponent(nama1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(13, 13, 13)
                .addGroup(page3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nim1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel58))
                .addGap(18, 18, 18)
                .addGroup(page3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel59)
                    .addGroup(page3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(no_hp1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(no_telp1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(2, 2, 2)
                .addGroup(page3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel98)
                    .addComponent(jLabel99))
                .addGap(2, 2, 2)
                .addGroup(page3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel60, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(email1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(page3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel61)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(31, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, page3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton23, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        body_edit.add(page3, "page1");

        page4.setBackground(new java.awt.Color(255, 255, 255));
        page4.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jLabel62.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel62.setText("Password  :");

        jLabel63.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel63.setText("Ulangi Password  :");

        jLabel64.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel64.setText("Tanggal Lahir  :");

        password1.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        password1.setToolTipText("Pastikan Capslock hidup atau tidak");

        ulang_password1.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N

        fakultas1.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        fakultas1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "INFORMATIKA", "TELEKOMUNIKASI" }));

        jLabel65.setBackground(new java.awt.Color(255, 255, 255));
        jLabel65.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel65.setText("Jurusan  :");

        jButton24.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jButton24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/check.png"))); // NOI18N
        jButton24.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton24ActionPerformed(evt);
            }
        });

        jButton25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/go-back-icon.png"))); // NOI18N
        jButton25.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton25ActionPerformed(evt);
            }
        });

        jLabel67.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel67.setText("Angkatan  : ");

        tahun1.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        tahun1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "2007", "2008", "2009", "2010", "2011", "2012" }));

        jLabel68.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel68.setText("Pembimbing  :");

        pembimbing1.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N

        jLabel100.setFont(new java.awt.Font("Calibri", 2, 12)); // NOI18N
        jLabel100.setText("Min. 6 karakter");

        jDateChooser2.setDateFormatString("yyyy - MM - DD");
        jDateChooser2.setMaxSelectableDate(new java.util.Date(788896901000L));
        jDateChooser2.setMinSelectableDate(new java.util.Date(473364101000L));

        jYearChooser1.setYear(2010);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel64)
                            .addComponent(jLabel65)
                            .addComponent(jLabel62))
                        .addGap(24, 24, 24))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel63)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jYearChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(fakultas1, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(password1)
                        .addComponent(ulang_password1, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel100)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 59, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel68)
                    .addComponent(jLabel67))
                .addGap(36, 36, 36)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tahun1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pembimbing1, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(51, 51, 51))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton25, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButton24, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel65)
                    .addComponent(fakultas1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel67)
                    .addComponent(tahun1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jDateChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel64)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(pembimbing1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel68))
                    .addComponent(jYearChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel62)
                    .addComponent(password1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel100)
                        .addGap(10, 10, 10)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ulang_password1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel63, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 64, Short.MAX_VALUE)
                        .addComponent(jButton25))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton24, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        javax.swing.GroupLayout page4Layout = new javax.swing.GroupLayout(page4);
        page4.setLayout(page4Layout);
        page4Layout.setHorizontalGroup(
            page4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        page4Layout.setVerticalGroup(
            page4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        body_edit.add(page4, "page2");

        jButton29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/go-back-icon.png"))); // NOI18N
        jButton29.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton29ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout menu_edit_akunLayout = new javax.swing.GroupLayout(menu_edit_akun);
        menu_edit_akun.setLayout(menu_edit_akunLayout);
        menu_edit_akunLayout.setHorizontalGroup(
            menu_edit_akunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(menu_edit_akunLayout.createSequentialGroup()
                .addGap(194, 194, 194)
                .addComponent(jButton29, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(750, Short.MAX_VALUE))
            .addGroup(menu_edit_akunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, menu_edit_akunLayout.createSequentialGroup()
                    .addContainerGap(195, Short.MAX_VALUE)
                    .addComponent(body_edit, javax.swing.GroupLayout.PREFERRED_SIZE, 617, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(202, Short.MAX_VALUE)))
        );
        menu_edit_akunLayout.setVerticalGroup(
            menu_edit_akunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, menu_edit_akunLayout.createSequentialGroup()
                .addContainerGap(292, Short.MAX_VALUE)
                .addComponent(jButton29)
                .addContainerGap())
            .addGroup(menu_edit_akunLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(menu_edit_akunLayout.createSequentialGroup()
                    .addComponent(body_edit, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 49, Short.MAX_VALUE)))
        );

        body_mhs.add(menu_edit_akun, "edit_akun");

        javax.swing.GroupLayout Menu_mhsLayout = new javax.swing.GroupLayout(Menu_mhs);
        Menu_mhs.setLayout(Menu_mhsLayout);
        Menu_mhsLayout.setHorizontalGroup(
            Menu_mhsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Menu_mhsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Menu_mhsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(header_mhs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(body_mhs, javax.swing.GroupLayout.PREFERRED_SIZE, 1014, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(31, Short.MAX_VALUE))
        );
        Menu_mhsLayout.setVerticalGroup(
            Menu_mhsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Menu_mhsLayout.createSequentialGroup()
                .addComponent(header_mhs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(body_mhs, javax.swing.GroupLayout.DEFAULT_SIZE, 336, Short.MAX_VALUE)
                .addContainerGap())
        );

        Login.add(Menu_mhs, "menu_mhs");

        Body.add(Login, "login");

        registrasi.setBackground(new java.awt.Color(255, 255, 255));
        registrasi.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        title_registrasi.setBackground(new java.awt.Color(255, 255, 255));

        jLabel23.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(0, 153, 204));
        jLabel23.setText("Form Pendaftaran");

        javax.swing.GroupLayout title_registrasiLayout = new javax.swing.GroupLayout(title_registrasi);
        title_registrasi.setLayout(title_registrasiLayout);
        title_registrasiLayout.setHorizontalGroup(
            title_registrasiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, title_registrasiLayout.createSequentialGroup()
                .addContainerGap(189, Short.MAX_VALUE)
                .addComponent(jLabel23)
                .addGap(41, 41, 41))
        );
        title_registrasiLayout.setVerticalGroup(
            title_registrasiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, title_registrasiLayout.createSequentialGroup()
                .addGap(0, 9, Short.MAX_VALUE)
                .addComponent(jLabel23))
        );

        body_registrasi.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));
        body_registrasi.setLayout(new java.awt.CardLayout());

        page1.setBackground(new java.awt.Color(0, 153, 204));
        page1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(51, 153, 255), 1, true));

        jLabel17.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setText("Nama * :");

        jLabel18.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setText("NIM * :");

        nama.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        nama.setToolTipText("");
        nama.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                namaKeyTyped(evt);
            }
        });

        nim.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        nim.setToolTipText("NIM");
        nim.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                nimKeyTyped(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setText("Nomor Telepon * :");

        no_hp.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        no_hp.setToolTipText("NoHP");
        no_hp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                no_hpKeyTyped(evt);
            }
        });

        no_telp.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        no_telp.setToolTipText("Telp");
        no_telp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                no_telpKeyTyped(evt);
            }
        });

        jLabel21.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(255, 255, 255));
        jLabel21.setText("Email * :");

        jLabel22.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setText("Alamat * :");

        email.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        email.setToolTipText("");
        email.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                emailKeyTyped(evt);
            }
        });

        alamat.setColumns(20);
        alamat.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        alamat.setRows(5);
        alamat.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                alamatKeyTyped(evt);
            }
        });
        jScrollPane2.setViewportView(alamat);

        jButton5.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/go-next-icon.png"))); // NOI18N
        jButton5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton5.setEnabled(false);
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jLabel43.setFont(new java.awt.Font("Calibri", 2, 12)); // NOI18N
        jLabel43.setForeground(new java.awt.Color(255, 255, 255));
        jLabel43.setText("* : wajib disi ");

        jLabel66.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        jLabel66.setForeground(new java.awt.Color(255, 255, 255));
        jLabel66.setText("/");

        jLabel93.setFont(new java.awt.Font("Calibri", 2, 12)); // NOI18N
        jLabel93.setForeground(new java.awt.Color(255, 255, 255));
        jLabel93.setText("Maks. 9 karakter");

        jLabel95.setFont(new java.awt.Font("Calibri", 2, 12)); // NOI18N
        jLabel95.setForeground(new java.awt.Color(255, 255, 255));
        jLabel95.setText("No HP");

        jLabel96.setFont(new java.awt.Font("Calibri", 2, 12)); // NOI18N
        jLabel96.setForeground(new java.awt.Color(255, 255, 255));
        jLabel96.setText("No telepon rumah");

        javax.swing.GroupLayout page1Layout = new javax.swing.GroupLayout(page1);
        page1.setLayout(page1Layout);
        page1Layout.setHorizontalGroup(
            page1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, page1Layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(jLabel43)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, page1Layout.createSequentialGroup()
                .addContainerGap(25, Short.MAX_VALUE)
                .addGroup(page1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel17)
                    .addComponent(jLabel20)
                    .addComponent(jLabel21)
                    .addComponent(jLabel22)
                    .addComponent(jLabel18))
                .addGap(42, 42, 42)
                .addGroup(page1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(page1Layout.createSequentialGroup()
                        .addComponent(nim, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel93)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, page1Layout.createSequentialGroup()
                        .addGroup(page1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(page1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(nama, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(email, javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, page1Layout.createSequentialGroup()
                                    .addGroup(page1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(page1Layout.createSequentialGroup()
                                            .addComponent(no_hp, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(jLabel66, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, page1Layout.createSequentialGroup()
                                            .addGap(10, 10, 10)
                                            .addComponent(jLabel95)
                                            .addGap(82, 82, 82)))
                                    .addGroup(page1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(page1Layout.createSequentialGroup()
                                            .addGap(10, 10, 10)
                                            .addComponent(jLabel96))
                                        .addComponent(no_telp, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGap(202, 202, 202))))
        );
        page1Layout.setVerticalGroup(
            page1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(page1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(page1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton5)
                    .addGroup(page1Layout.createSequentialGroup()
                        .addGroup(page1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel17)
                            .addComponent(nama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(13, 13, 13)
                        .addGroup(page1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nim, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel18)
                            .addComponent(jLabel93))
                        .addGap(18, 18, 18)
                        .addGroup(page1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel20)
                            .addGroup(page1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(no_hp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(no_telp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel66)))
                        .addGap(1, 1, 1)
                        .addGroup(page1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel95)
                            .addComponent(jLabel96))
                        .addGap(3, 3, 3)
                        .addGroup(page1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(19, 19, 19)
                        .addGroup(page1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel22)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel43, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        body_registrasi.add(page1, "page1");

        page2.setBackground(new java.awt.Color(0, 153, 204));
        page2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        jPanel3.setBackground(new java.awt.Color(0, 153, 204));
        jPanel3.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        jLabel26.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(255, 255, 255));
        jLabel26.setText("Password * :");

        jLabel27.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(255, 255, 255));
        jLabel27.setText("Ulangi Password * :");

        jLabel28.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel28.setForeground(new java.awt.Color(255, 255, 255));
        jLabel28.setText("Tanggal Lahir ** :");

        password.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        password.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                passwordKeyTyped(evt);
            }
        });

        ulang_password.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        ulang_password.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                ulang_passwordKeyTyped(evt);
            }
        });

        fakultas.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        fakultas.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "INFORMATIKA", "TELEKOMUNIKASI" }));

        jLabel29.setBackground(new java.awt.Color(255, 255, 255));
        jLabel29.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel29.setForeground(new java.awt.Color(255, 255, 255));
        jLabel29.setText("Jurusan ** :");

        jButton3.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/check.png"))); // NOI18N
        jButton3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton3.setEnabled(false);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jCheckBox1.setBackground(new java.awt.Color(0, 153, 204));
        jCheckBox1.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jCheckBox1.setForeground(new java.awt.Color(255, 255, 255));
        jCheckBox1.setText("Saya bersedia memenuhi persyaratan yang ada. Jika saya melanggar, saya bersedia menerima sangsi");
        jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1ActionPerformed(evt);
            }
        });

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/go-back-icon.png"))); // NOI18N
        jButton6.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jLabel44.setFont(new java.awt.Font("Calibri", 2, 12)); // NOI18N
        jLabel44.setForeground(new java.awt.Color(255, 255, 255));
        jLabel44.setText("* : wajib disi ");

        jLabel30.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel30.setForeground(new java.awt.Color(255, 255, 255));
        jLabel30.setText("Angkatan ** : ");

        tahun.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        tahun.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012" }));

        jLabel46.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel46.setForeground(new java.awt.Color(255, 255, 255));
        jLabel46.setText("Pembimbing ** :");

        pembimbing.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N

        jLabel84.setFont(new java.awt.Font("Calibri", 2, 12)); // NOI18N
        jLabel84.setForeground(new java.awt.Color(255, 255, 255));
        jLabel84.setText("** : wajib dipilih ");

        jLabel94.setFont(new java.awt.Font("Calibri", 2, 12)); // NOI18N
        jLabel94.setForeground(new java.awt.Color(255, 255, 255));
        jLabel94.setText("Min. 6 karakter");

        jDateChooser1.setMaxSelectableDate(new java.util.Date(788896909000L));
        jDateChooser1.setMinSelectableDate(new java.util.Date(473364109000L));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel28)
                            .addComponent(jLabel26)
                            .addComponent(jLabel27)
                            .addComponent(jLabel29))
                        .addGap(24, 24, 24)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(fakultas, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(password, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                                    .addComponent(ulang_password))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel46)
                                    .addComponent(jLabel30))
                                .addGap(18, 18, 18))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jLabel94)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tahun, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pembimbing, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(34, 34, 34))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel84)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel44)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(21, 21, 21))
                            .addComponent(jCheckBox1, javax.swing.GroupLayout.DEFAULT_SIZE, 571, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel29)
                            .addComponent(fakultas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel30))
                        .addGap(20, 20, 20)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel28)
                                .addComponent(jLabel46))
                            .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(26, 26, 26)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel26))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel94))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(tahun, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(pembimbing, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ulang_password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jCheckBox1)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addComponent(jButton6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel44, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel84)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout page2Layout = new javax.swing.GroupLayout(page2);
        page2.setLayout(page2Layout);
        page2Layout.setHorizontalGroup(
            page2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        page2Layout.setVerticalGroup(
            page2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        body_registrasi.add(page2, "page2");

        jButton9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/go-back-icon.png"))); // NOI18N
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout registrasiLayout = new javax.swing.GroupLayout(registrasi);
        registrasi.setLayout(registrasiLayout);
        registrasiLayout.setHorizontalGroup(
            registrasiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(registrasiLayout.createSequentialGroup()
                .addGroup(registrasiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(registrasiLayout.createSequentialGroup()
                        .addGap(91, 91, 91)
                        .addComponent(title_registrasi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(registrasiLayout.createSequentialGroup()
                        .addGap(261, 261, 261)
                        .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(594, Short.MAX_VALUE))
            .addGroup(registrasiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(registrasiLayout.createSequentialGroup()
                    .addGap(260, 260, 260)
                    .addComponent(body_registrasi, javax.swing.GroupLayout.PREFERRED_SIZE, 611, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(227, Short.MAX_VALUE)))
        );
        registrasiLayout.setVerticalGroup(
            registrasiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(registrasiLayout.createSequentialGroup()
                .addComponent(title_registrasi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 313, Short.MAX_VALUE)
                .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(registrasiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(registrasiLayout.createSequentialGroup()
                    .addGap(41, 41, 41)
                    .addComponent(body_registrasi, javax.swing.GroupLayout.PREFERRED_SIZE, 302, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(55, Short.MAX_VALUE)))
        );

        Body.add(registrasi, "registrasi");

        lupa_pass.setBackground(new java.awt.Color(255, 255, 255));
        lupa_pass.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        title_lupa.setBackground(new java.awt.Color(255, 255, 255));

        jLabel15.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(0, 153, 204));
        jLabel15.setText("Bantuan Login");

        javax.swing.GroupLayout title_lupaLayout = new javax.swing.GroupLayout(title_lupa);
        title_lupa.setLayout(title_lupaLayout);
        title_lupaLayout.setHorizontalGroup(
            title_lupaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(title_lupaLayout.createSequentialGroup()
                .addGap(252, 252, 252)
                .addComponent(jLabel15)
                .addContainerGap(680, Short.MAX_VALUE))
        );
        title_lupaLayout.setVerticalGroup(
            title_lupaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, title_lupaLayout.createSequentialGroup()
                .addGap(0, 34, Short.MAX_VALUE)
                .addComponent(jLabel15))
        );

        body_lupa.setBackground(new java.awt.Color(0, 153, 204));
        body_lupa.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        jLabel31.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jLabel31.setForeground(new java.awt.Color(255, 255, 255));
        jLabel31.setText("Masukkan Email : ");

        jTextField8.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jTextField8.setToolTipText("");
        jTextField8.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextField8KeyTyped(evt);
            }
        });

        jButton7.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jButton7.setText("Kirim");
        jButton7.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton7.setEnabled(false);
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout body_lupaLayout = new javax.swing.GroupLayout(body_lupa);
        body_lupa.setLayout(body_lupaLayout);
        body_lupaLayout.setHorizontalGroup(
            body_lupaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(body_lupaLayout.createSequentialGroup()
                .addGap(111, 111, 111)
                .addGroup(body_lupaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(body_lupaLayout.createSequentialGroup()
                        .addComponent(jLabel31)
                        .addGap(31, 31, 31)
                        .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(138, Short.MAX_VALUE))
        );
        body_lupaLayout.setVerticalGroup(
            body_lupaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(body_lupaLayout.createSequentialGroup()
                .addGap(72, 72, 72)
                .addGroup(body_lupaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel31)
                    .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton7)
                .addContainerGap(142, Short.MAX_VALUE))
        );

        jButton10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/go-back-icon.png"))); // NOI18N
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout lupa_passLayout = new javax.swing.GroupLayout(lupa_pass);
        lupa_pass.setLayout(lupa_passLayout);
        lupa_passLayout.setHorizontalGroup(
            lupa_passLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(lupa_passLayout.createSequentialGroup()
                .addGap(244, 244, 244)
                .addGroup(lupa_passLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(body_lupa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(239, Short.MAX_VALUE))
            .addGroup(lupa_passLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, lupa_passLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(title_lupa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        lupa_passLayout.setVerticalGroup(
            lupa_passLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, lupa_passLayout.createSequentialGroup()
                .addContainerGap(63, Short.MAX_VALUE)
                .addComponent(body_lupa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton10)
                .addGap(20, 20, 20))
            .addGroup(lupa_passLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(lupa_passLayout.createSequentialGroup()
                    .addComponent(title_lupa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 334, Short.MAX_VALUE)))
        );

        Body.add(lupa_pass, "lupas_pass");

        Menu_cari.setBackground(new java.awt.Color(255, 255, 255));
        Menu_cari.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        title_cari.setBackground(new java.awt.Color(255, 255, 255));

        jLabel33.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel33.setForeground(new java.awt.Color(0, 153, 204));
        jLabel33.setText("Cari Proyek Akhir");

        javax.swing.GroupLayout title_cariLayout = new javax.swing.GroupLayout(title_cari);
        title_cari.setLayout(title_cariLayout);
        title_cariLayout.setHorizontalGroup(
            title_cariLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(title_cariLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jLabel33)
                .addContainerGap(559, Short.MAX_VALUE))
        );
        title_cariLayout.setVerticalGroup(
            title_cariLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, title_cariLayout.createSequentialGroup()
                .addGap(0, 24, Short.MAX_VALUE)
                .addComponent(jLabel33))
        );

        body_cari.setBackground(new java.awt.Color(255, 255, 255));

        cari_kiri.setBackground(new java.awt.Color(255, 255, 255));
        cari_kiri.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));
        cari_kiri.setMaximumSize(new java.awt.Dimension(397, 178));

        jComboBox6.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jComboBox6.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "NIM", "JUDUL PA", "NAMA FAKULTAS" }));
        jComboBox6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox6ActionPerformed(evt);
            }
        });

        jLabel35.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel35.setText("Cari berdasarkan :");

        jTextField9.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N

        jLabel36.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel36.setText("Masukkan NIM : ");

        jButton12.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jButton12.setText("Cari");
        jButton12.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout cari_kiriLayout = new javax.swing.GroupLayout(cari_kiri);
        cari_kiri.setLayout(cari_kiriLayout);
        cari_kiriLayout.setHorizontalGroup(
            cari_kiriLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cari_kiriLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(cari_kiriLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton12, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(cari_kiriLayout.createSequentialGroup()
                        .addGroup(cari_kiriLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel35)
                            .addComponent(jLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(cari_kiriLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBox6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(66, Short.MAX_VALUE))
        );
        cari_kiriLayout.setVerticalGroup(
            cari_kiriLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cari_kiriLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(cari_kiriLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35))
                .addGap(37, 37, 37)
                .addGroup(cari_kiriLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel36))
                .addGap(18, 18, 18)
                .addComponent(jButton12)
                .addContainerGap(120, Short.MAX_VALUE))
        );

        cari_kanan.setBackground(new java.awt.Color(255, 255, 255));
        cari_kanan.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        jTable2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 1, true));
        jTable2.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID PA", "Nama Mahasiswa", "NIM", "Judul PA", "Nama Pembimbing", "Fakultas"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable2MouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(jTable2);

        jComboBox5.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jComboBox5.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "NIM", "Judul PA" }));
        jComboBox5.setEnabled(false);
        jComboBox5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox5ActionPerformed(evt);
            }
        });

        jLabel34.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel34.setText("Urutkan berdasarkan :");

        javax.swing.GroupLayout cari_kananLayout = new javax.swing.GroupLayout(cari_kanan);
        cari_kanan.setLayout(cari_kananLayout);
        cari_kananLayout.setHorizontalGroup(
            cari_kananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cari_kananLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jLabel34)
                .addGap(18, 18, 18)
                .addComponent(jComboBox5, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 546, Short.MAX_VALUE)
        );
        cari_kananLayout.setVerticalGroup(
            cari_kananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, cari_kananLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(cari_kananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel34)
                    .addComponent(jComboBox5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jButton13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/go-back-icon.png"))); // NOI18N
        jButton13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton13ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout body_cariLayout = new javax.swing.GroupLayout(body_cari);
        body_cari.setLayout(body_cariLayout);
        body_cariLayout.setHorizontalGroup(
            body_cariLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(body_cariLayout.createSequentialGroup()
                .addComponent(cari_kiri, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(cari_kanan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(26, 26, 26))
            .addGroup(body_cariLayout.createSequentialGroup()
                .addComponent(jButton13, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(907, Short.MAX_VALUE))
        );
        body_cariLayout.setVerticalGroup(
            body_cariLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(body_cariLayout.createSequentialGroup()
                .addGroup(body_cariLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cari_kiri, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cari_kanan, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton13, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout Menu_cariLayout = new javax.swing.GroupLayout(Menu_cari);
        Menu_cari.setLayout(Menu_cariLayout);
        Menu_cariLayout.setHorizontalGroup(
            Menu_cariLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Menu_cariLayout.createSequentialGroup()
                .addGap(85, 85, 85)
                .addGroup(Menu_cariLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(title_cari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(body_cari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(36, Short.MAX_VALUE))
        );
        Menu_cariLayout.setVerticalGroup(
            Menu_cariLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Menu_cariLayout.createSequentialGroup()
                .addComponent(title_cari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(body_cari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        Body.add(Menu_cari, "cari");

        pilih_user.setBackground(new java.awt.Color(255, 255, 255));
        pilih_user.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        body_pilih.setBackground(new java.awt.Color(0, 153, 204));
        body_pilih.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        jButton14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/user-blue-64.png"))); // NOI18N
        jButton14.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton14ActionPerformed(evt);
            }
        });

        jButton15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/mahasiswa.png"))); // NOI18N
        jButton15.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton15ActionPerformed(evt);
            }
        });

        jLabel53.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel53.setForeground(new java.awt.Color(255, 255, 255));
        jLabel53.setText("Admin");

        jLabel54.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel54.setForeground(new java.awt.Color(255, 255, 255));
        jLabel54.setText("Mahasiswa");

        javax.swing.GroupLayout body_pilihLayout = new javax.swing.GroupLayout(body_pilih);
        body_pilih.setLayout(body_pilihLayout);
        body_pilihLayout.setHorizontalGroup(
            body_pilihLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(body_pilihLayout.createSequentialGroup()
                .addGap(150, 150, 150)
                .addComponent(jButton14, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(69, 69, 69)
                .addComponent(jButton15, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(148, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, body_pilihLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel53)
                .addGap(141, 141, 141)
                .addComponent(jLabel54)
                .addGap(174, 174, 174))
        );
        body_pilihLayout.setVerticalGroup(
            body_pilihLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(body_pilihLayout.createSequentialGroup()
                .addGap(80, 80, 80)
                .addGroup(body_pilihLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton14, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton15, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(body_pilihLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel53)
                    .addComponent(jLabel54))
                .addContainerGap(66, Short.MAX_VALUE))
        );

        title_pilih.setBackground(new java.awt.Color(255, 255, 255));

        jLabel37.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel37.setForeground(new java.awt.Color(0, 153, 204));
        jLabel37.setText("Pilih User");

        javax.swing.GroupLayout title_pilihLayout = new javax.swing.GroupLayout(title_pilih);
        title_pilih.setLayout(title_pilihLayout);
        title_pilihLayout.setHorizontalGroup(
            title_pilihLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(title_pilihLayout.createSequentialGroup()
                .addGap(103, 103, 103)
                .addComponent(jLabel37)
                .addContainerGap(290, Short.MAX_VALUE))
        );
        title_pilihLayout.setVerticalGroup(
            title_pilihLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(title_pilihLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel37)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pilih_userLayout = new javax.swing.GroupLayout(pilih_user);
        pilih_user.setLayout(pilih_userLayout);
        pilih_userLayout.setHorizontalGroup(
            pilih_userLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pilih_userLayout.createSequentialGroup()
                .addGroup(pilih_userLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pilih_userLayout.createSequentialGroup()
                        .addGap(164, 164, 164)
                        .addComponent(title_pilih, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pilih_userLayout.createSequentialGroup()
                        .addGap(247, 247, 247)
                        .addComponent(body_pilih, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(246, Short.MAX_VALUE))
        );
        pilih_userLayout.setVerticalGroup(
            pilih_userLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pilih_userLayout.createSequentialGroup()
                .addContainerGap(36, Short.MAX_VALUE)
                .addComponent(title_pilih, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(body_pilih, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(64, 64, 64))
        );

        Body.add(pilih_user, "pilih_user");

        Admin.setBackground(new java.awt.Color(255, 255, 255));
        Admin.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));
        Admin.setLayout(new java.awt.CardLayout());

        body_admin.setBackground(new java.awt.Color(255, 255, 255));

        title_login.setBackground(new java.awt.Color(255, 255, 255));

        jLabel42.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel42.setForeground(new java.awt.Color(0, 153, 204));
        jLabel42.setText("Form Login");

        javax.swing.GroupLayout title_loginLayout = new javax.swing.GroupLayout(title_login);
        title_login.setLayout(title_loginLayout);
        title_loginLayout.setHorizontalGroup(
            title_loginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(title_loginLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jLabel42)
                .addContainerGap(258, Short.MAX_VALUE))
        );
        title_loginLayout.setVerticalGroup(
            title_loginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel42, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        login_admin.setBackground(new java.awt.Color(0, 153, 204));
        login_admin.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        jLabel38.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jLabel38.setForeground(new java.awt.Color(255, 255, 255));
        jLabel38.setText("Username");

        jLabel39.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jLabel39.setForeground(new java.awt.Color(255, 255, 255));
        jLabel39.setText("Password");

        jTextField10.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jTextField10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField10ActionPerformed(evt);
            }
        });

        jPasswordField4.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jPasswordField4.setToolTipText("Pastikan Capslock hidup atau tidak");

        jButton16.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jButton16.setText("Masuk");
        jButton16.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton16ActionPerformed(evt);
            }
        });

        jLabel41.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jLabel41.setForeground(new java.awt.Color(255, 255, 255));
        jLabel41.setText("Bantuan Login");
        jLabel41.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel41.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel41MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout login_adminLayout = new javax.swing.GroupLayout(login_admin);
        login_admin.setLayout(login_adminLayout);
        login_adminLayout.setHorizontalGroup(
            login_adminLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(login_adminLayout.createSequentialGroup()
                .addGap(172, 172, 172)
                .addGroup(login_adminLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel41)
                    .addGroup(login_adminLayout.createSequentialGroup()
                        .addGroup(login_adminLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel39)
                            .addComponent(jLabel38))
                        .addGap(98, 98, 98)
                        .addGroup(login_adminLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPasswordField4, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton16, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(0, 224, Short.MAX_VALUE))
        );
        login_adminLayout.setVerticalGroup(
            login_adminLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, login_adminLayout.createSequentialGroup()
                .addContainerGap(47, Short.MAX_VALUE)
                .addGroup(login_adminLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel38)
                    .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(login_adminLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jPasswordField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel39))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton16, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(jLabel41)
                .addGap(51, 51, 51))
        );

        jButton17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/go-back-icon.png"))); // NOI18N
        jButton17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton17ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout body_adminLayout = new javax.swing.GroupLayout(body_admin);
        body_admin.setLayout(body_adminLayout);
        body_adminLayout.setHorizontalGroup(
            body_adminLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(body_adminLayout.createSequentialGroup()
                .addGap(192, 192, 192)
                .addGroup(body_adminLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton17, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(login_admin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(title_login, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(177, Short.MAX_VALUE))
        );
        body_adminLayout.setVerticalGroup(
            body_adminLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(body_adminLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(title_login, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(login_admin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton17, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(51, Short.MAX_VALUE))
        );

        Admin.add(body_admin, "login_admin");

        masuk_admin.setBackground(new java.awt.Color(255, 255, 255));

        title_masuk.setBackground(new java.awt.Color(255, 255, 255));

        jLabel45.setFont(new java.awt.Font("Calibri", 3, 14)); // NOI18N
        jLabel45.setForeground(new java.awt.Color(0, 153, 204));
        jLabel45.setText("Anda Masuk Sebagai Admin");

        jButton18.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jButton18.setText("Log Out");
        jButton18.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton18ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout title_masukLayout = new javax.swing.GroupLayout(title_masuk);
        title_masuk.setLayout(title_masukLayout);
        title_masukLayout.setHorizontalGroup(
            title_masukLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(title_masukLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel45)
                .addGap(46, 46, 46)
                .addComponent(jButton18, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(208, 208, 208))
        );
        title_masukLayout.setVerticalGroup(
            title_masukLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(title_masukLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(title_masukLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton18, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel45)))
        );

        body_masuk.setBackground(new java.awt.Color(255, 255, 255));
        body_masuk.setLayout(new java.awt.CardLayout());

        menu_utama_admin.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(0, 153, 204));
        jPanel2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        jButton27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/tambah data2.png"))); // NOI18N
        jButton27.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton27.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton27ActionPerformed(evt);
            }
        });

        jButton34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/edit database1.png"))); // NOI18N
        jButton34.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton34.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton34ActionPerformed(evt);
            }
        });

        jLabel70.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel70.setForeground(new java.awt.Color(255, 255, 255));
        jLabel70.setText("Input Data");

        jLabel71.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel71.setForeground(new java.awt.Color(255, 255, 255));
        jLabel71.setText("Edit Data");

        jButton28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/download data3.png"))); // NOI18N
        jButton28.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton28.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton28ActionPerformed(evt);
            }
        });

        jLabel82.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel82.setForeground(new java.awt.Color(255, 255, 255));
        jLabel82.setText("Download PA");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(66, 66, 66)
                .addComponent(jButton27, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(58, 58, 58)
                .addComponent(jButton34, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(57, 57, 57)
                .addComponent(jButton28, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(71, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(119, 119, 119)
                .addComponent(jLabel70)
                .addGap(179, 179, 179)
                .addComponent(jLabel71)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel82)
                .addGap(120, 120, 120))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(49, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton27, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton34, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton28, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel71)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel70)
                        .addComponent(jLabel82)))
                .addGap(42, 42, 42))
        );

        javax.swing.GroupLayout menu_utama_adminLayout = new javax.swing.GroupLayout(menu_utama_admin);
        menu_utama_admin.setLayout(menu_utama_adminLayout);
        menu_utama_adminLayout.setHorizontalGroup(
            menu_utama_adminLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(menu_utama_adminLayout.createSequentialGroup()
                .addGap(67, 67, 67)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(257, Short.MAX_VALUE))
        );
        menu_utama_adminLayout.setVerticalGroup(
            menu_utama_adminLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(menu_utama_adminLayout.createSequentialGroup()
                .addGap(47, 47, 47)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(44, Short.MAX_VALUE))
        );

        body_masuk.add(menu_utama_admin, "utama_admin");

        input_data.setBackground(new java.awt.Color(255, 255, 255));

        header_input.setBackground(new java.awt.Color(255, 255, 255));

        jLabel52.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel52.setForeground(new java.awt.Color(0, 153, 204));
        jLabel52.setText("Input Pembimbing");

        javax.swing.GroupLayout header_inputLayout = new javax.swing.GroupLayout(header_input);
        header_input.setLayout(header_inputLayout);
        header_inputLayout.setHorizontalGroup(
            header_inputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(header_inputLayout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(jLabel52)
                .addContainerGap(360, Short.MAX_VALUE))
        );
        header_inputLayout.setVerticalGroup(
            header_inputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(header_inputLayout.createSequentialGroup()
                .addComponent(jLabel52)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        body_input.setBackground(new java.awt.Color(255, 255, 255));

        input_dosen.setBackground(new java.awt.Color(255, 255, 255));
        input_dosen.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        jLabel73.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel73.setText("Nama Pembimbing : ");

        jLabel74.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel74.setText("NIP :");

        jTextField2.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jTextField2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextField2KeyTyped(evt);
            }
        });

        jTextField3.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jTextField3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextField3KeyTyped(evt);
            }
        });

        jButton38.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jButton38.setText("Simpan");
        jButton38.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton38.setEnabled(false);
        jButton38.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton38ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout input_dosenLayout = new javax.swing.GroupLayout(input_dosen);
        input_dosen.setLayout(input_dosenLayout);
        input_dosenLayout.setHorizontalGroup(
            input_dosenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, input_dosenLayout.createSequentialGroup()
                .addContainerGap(104, Short.MAX_VALUE)
                .addGroup(input_dosenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton38, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(input_dosenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(input_dosenLayout.createSequentialGroup()
                            .addComponent(jLabel74)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(input_dosenLayout.createSequentialGroup()
                            .addComponent(jLabel73)
                            .addGap(18, 18, 18)
                            .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(110, 110, 110))
        );
        input_dosenLayout.setVerticalGroup(
            input_dosenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(input_dosenLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(input_dosenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel73))
                .addGap(32, 32, 32)
                .addGroup(input_dosenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel74))
                .addGap(18, 18, 18)
                .addComponent(jButton38, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout body_inputLayout = new javax.swing.GroupLayout(body_input);
        body_input.setLayout(body_inputLayout);
        body_inputLayout.setHorizontalGroup(
            body_inputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(body_inputLayout.createSequentialGroup()
                .addGap(0, 32, Short.MAX_VALUE)
                .addComponent(input_dosen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 32, Short.MAX_VALUE))
        );
        body_inputLayout.setVerticalGroup(
            body_inputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(body_inputLayout.createSequentialGroup()
                .addGap(0, 5, Short.MAX_VALUE)
                .addComponent(input_dosen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 6, Short.MAX_VALUE))
        );

        footer_input.setBackground(new java.awt.Color(255, 255, 255));

        jButton42.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/go-back-icon.png"))); // NOI18N
        jButton42.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton42.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton42ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout footer_inputLayout = new javax.swing.GroupLayout(footer_input);
        footer_input.setLayout(footer_inputLayout);
        footer_inputLayout.setHorizontalGroup(
            footer_inputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(footer_inputLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton42, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(82, Short.MAX_VALUE))
        );
        footer_inputLayout.setVerticalGroup(
            footer_inputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(footer_inputLayout.createSequentialGroup()
                .addComponent(jButton42)
                .addGap(0, 10, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout input_dataLayout = new javax.swing.GroupLayout(input_data);
        input_data.setLayout(input_dataLayout);
        input_dataLayout.setHorizontalGroup(
            input_dataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(input_dataLayout.createSequentialGroup()
                .addGroup(input_dataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(input_dataLayout.createSequentialGroup()
                        .addGap(142, 142, 142)
                        .addGroup(input_dataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(header_input, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(body_input, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(input_dataLayout.createSequentialGroup()
                        .addGap(163, 163, 163)
                        .addComponent(footer_input, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(365, Short.MAX_VALUE))
        );
        input_dataLayout.setVerticalGroup(
            input_dataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(input_dataLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(header_input, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(body_input, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(footer_input, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(38, Short.MAX_VALUE))
        );

        body_masuk.add(input_data, "input_data");

        edit_data.setBackground(new java.awt.Color(255, 255, 255));

        header_edit.setBackground(new java.awt.Color(255, 255, 255));

        jLabel81.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel81.setText("Edit :");

        jComboBox2.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Dosen Pembimbing", "Data PA" }));
        jComboBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox2ActionPerformed(evt);
            }
        });

        jLabel83.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel83.setText("Masukkan NIP :");

        jTextField13.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jTextField13.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextField13KeyTyped(evt);
            }
        });

        jButton26.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jButton26.setText("Cari");
        jButton26.setEnabled(false);
        jButton26.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton26ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout header_editLayout = new javax.swing.GroupLayout(header_edit);
        header_edit.setLayout(header_editLayout);
        header_editLayout.setHorizontalGroup(
            header_editLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(header_editLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel81, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(jLabel83, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButton26)
                .addContainerGap(108, Short.MAX_VALUE))
        );
        header_editLayout.setVerticalGroup(
            header_editLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(header_editLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel81)
                .addComponent(jLabel83)
                .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jButton26))
        );

        footer_edit.setBackground(new java.awt.Color(255, 255, 255));

        jButton43.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/go-back-icon.png"))); // NOI18N
        jButton43.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton43.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton43ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout footer_editLayout = new javax.swing.GroupLayout(footer_edit);
        footer_edit.setLayout(footer_editLayout);
        footer_editLayout.setHorizontalGroup(
            footer_editLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(footer_editLayout.createSequentialGroup()
                .addComponent(jButton43, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 32, Short.MAX_VALUE))
        );
        footer_editLayout.setVerticalGroup(
            footer_editLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, footer_editLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jButton43))
        );

        body_edit_data.setBackground(new java.awt.Color(255, 255, 255));

        body_edit_data_kiri.setBackground(new java.awt.Color(255, 255, 255));
        body_edit_data_kiri.setLayout(new java.awt.CardLayout());

        edit_pa.setBackground(new java.awt.Color(255, 255, 255));
        edit_pa.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        jLabel77.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel77.setText("Judul PA :");

        jLabel78.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel78.setText("ID PA :");

        jTextField6.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jTextField6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextField6KeyTyped(evt);
            }
        });

        jTextField7.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jTextField7.setEnabled(false);

        jButton40.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jButton40.setText("Simpan");
        jButton40.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton40.setEnabled(false);
        jButton40.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton40ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout edit_paLayout = new javax.swing.GroupLayout(edit_pa);
        edit_pa.setLayout(edit_paLayout);
        edit_paLayout.setHorizontalGroup(
            edit_paLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(edit_paLayout.createSequentialGroup()
                .addGap(82, 82, 82)
                .addGroup(edit_paLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton40, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(edit_paLayout.createSequentialGroup()
                        .addGroup(edit_paLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel77)
                            .addComponent(jLabel78))
                        .addGap(18, 18, 18)
                        .addGroup(edit_paLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(92, Short.MAX_VALUE))
        );
        edit_paLayout.setVerticalGroup(
            edit_paLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(edit_paLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(edit_paLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel77)
                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addGroup(edit_paLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel78)
                    .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton40, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(43, Short.MAX_VALUE))
        );

        body_edit_data_kiri.add(edit_pa, "edit_pa");

        edit_dosen.setBackground(new java.awt.Color(255, 255, 255));
        edit_dosen.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        jLabel79.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel79.setText("Nama Pembimbing : ");

        jLabel80.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel80.setText("NIP :");

        jTextField11.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jTextField11.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextField11KeyTyped(evt);
            }
        });

        jTextField12.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jTextField12.setEnabled(false);

        jButton41.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jButton41.setText("Simpan");
        jButton41.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton41.setEnabled(false);
        jButton41.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton41ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout edit_dosenLayout = new javax.swing.GroupLayout(edit_dosen);
        edit_dosen.setLayout(edit_dosenLayout);
        edit_dosenLayout.setHorizontalGroup(
            edit_dosenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, edit_dosenLayout.createSequentialGroup()
                .addGap(0, 32, Short.MAX_VALUE)
                .addGroup(edit_dosenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, edit_dosenLayout.createSequentialGroup()
                        .addComponent(jLabel79)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(84, 84, 84))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, edit_dosenLayout.createSequentialGroup()
                        .addComponent(jLabel80)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(edit_dosenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextField12, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton41, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(85, 85, 85))))
        );
        edit_dosenLayout.setVerticalGroup(
            edit_dosenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(edit_dosenLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(edit_dosenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel79))
                .addGap(32, 32, 32)
                .addGroup(edit_dosenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel80))
                .addGap(18, 18, 18)
                .addComponent(jButton41, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(28, Short.MAX_VALUE))
        );

        body_edit_data_kiri.add(edit_dosen, "edit_dosen");

        body_edit_data_kanan.setBackground(new java.awt.Color(255, 255, 255));

        jTable4.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jTable4.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Nama Pembimbing", "NIP"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable4MouseClicked(evt);
            }
        });
        jScrollPane8.setViewportView(jTable4);
        jTable4.getColumnModel().getColumn(0).setResizable(false);

        javax.swing.GroupLayout body_edit_data_kananLayout = new javax.swing.GroupLayout(body_edit_data_kanan);
        body_edit_data_kanan.setLayout(body_edit_data_kananLayout);
        body_edit_data_kananLayout.setHorizontalGroup(
            body_edit_data_kananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE)
        );
        body_edit_data_kananLayout.setVerticalGroup(
            body_edit_data_kananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout body_edit_dataLayout = new javax.swing.GroupLayout(body_edit_data);
        body_edit_data.setLayout(body_edit_dataLayout);
        body_edit_dataLayout.setHorizontalGroup(
            body_edit_dataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, body_edit_dataLayout.createSequentialGroup()
                .addGap(0, 435, Short.MAX_VALUE)
                .addComponent(body_edit_data_kanan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(body_edit_dataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(body_edit_dataLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(body_edit_data_kiri, javax.swing.GroupLayout.PREFERRED_SIZE, 405, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(453, Short.MAX_VALUE)))
        );
        body_edit_dataLayout.setVerticalGroup(
            body_edit_dataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(body_edit_dataLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(body_edit_data_kanan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(body_edit_dataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(body_edit_dataLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(body_edit_data_kiri, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(13, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout edit_dataLayout = new javax.swing.GroupLayout(edit_data);
        edit_data.setLayout(edit_dataLayout);
        edit_dataLayout.setHorizontalGroup(
            edit_dataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(edit_dataLayout.createSequentialGroup()
                .addGroup(edit_dataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(edit_dataLayout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(header_edit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(edit_dataLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(edit_dataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(edit_dataLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(footer_edit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(body_edit_data, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(222, Short.MAX_VALUE))
        );
        edit_dataLayout.setVerticalGroup(
            edit_dataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(edit_dataLayout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(header_edit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(body_edit_data, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(footer_edit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(24, Short.MAX_VALUE))
        );

        body_masuk.add(edit_data, "edit_data");

        download_data.setBackground(new java.awt.Color(255, 255, 255));

        header_download.setBackground(new java.awt.Color(255, 255, 255));

        jLabel69.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel69.setForeground(new java.awt.Color(0, 153, 204));
        jLabel69.setText("Download Proyek Akhir");

        javax.swing.GroupLayout header_downloadLayout = new javax.swing.GroupLayout(header_download);
        header_download.setLayout(header_downloadLayout);
        header_downloadLayout.setHorizontalGroup(
            header_downloadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(header_downloadLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel69)
                .addContainerGap(469, Short.MAX_VALUE))
        );
        header_downloadLayout.setVerticalGroup(
            header_downloadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, header_downloadLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel69))
        );

        body_download.setBackground(new java.awt.Color(255, 255, 255));

        kiri_body.setBackground(new java.awt.Color(255, 255, 255));
        kiri_body.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        jLabel85.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel85.setText("Masukkan ID PA : ");

        jTextField14.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jTextField14.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextField14KeyTyped(evt);
            }
        });

        jButton31.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jButton31.setText("Download");
        jButton31.setEnabled(false);
        jButton31.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton31ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout kiri_bodyLayout = new javax.swing.GroupLayout(kiri_body);
        kiri_body.setLayout(kiri_bodyLayout);
        kiri_bodyLayout.setHorizontalGroup(
            kiri_bodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, kiri_bodyLayout.createSequentialGroup()
                .addContainerGap(52, Short.MAX_VALUE)
                .addGroup(kiri_bodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton31)
                    .addGroup(kiri_bodyLayout.createSequentialGroup()
                        .addComponent(jLabel85)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField14, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(65, 65, 65))
        );
        kiri_bodyLayout.setVerticalGroup(
            kiri_bodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(kiri_bodyLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(kiri_bodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel85))
                .addGap(18, 18, 18)
                .addComponent(jButton31, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(71, Short.MAX_VALUE))
        );

        kanan_body.setBackground(new java.awt.Color(255, 255, 255));
        kanan_body.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        jTable3.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable3.setColumnSelectionAllowed(true);
        jTable3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable3MouseClicked(evt);
            }
        });
        jScrollPane6.setViewportView(jTable3);
        jTable3.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        jLabel86.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel86.setText("Urutkan berdasarkan :");

        jComboBox3.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "NIM", "ID PA" }));
        jComboBox3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout kanan_bodyLayout = new javax.swing.GroupLayout(kanan_body);
        kanan_body.setLayout(kanan_bodyLayout);
        kanan_bodyLayout.setHorizontalGroup(
            kanan_bodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE)
            .addGroup(kanan_bodyLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jLabel86)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        kanan_bodyLayout.setVerticalGroup(
            kanan_bodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, kanan_bodyLayout.createSequentialGroup()
                .addGap(0, 3, Short.MAX_VALUE)
                .addGroup(kanan_bodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel86)
                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout body_downloadLayout = new javax.swing.GroupLayout(body_download);
        body_download.setLayout(body_downloadLayout);
        body_downloadLayout.setHorizontalGroup(
            body_downloadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(body_downloadLayout.createSequentialGroup()
                .addComponent(kiri_body, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(kanan_body, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        body_downloadLayout.setVerticalGroup(
            body_downloadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(body_downloadLayout.createSequentialGroup()
                .addGroup(body_downloadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(kiri_body, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(kanan_body, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(8, 8, 8))
        );

        jButton30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/go-back-icon.png"))); // NOI18N
        jButton30.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton30ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout download_dataLayout = new javax.swing.GroupLayout(download_data);
        download_data.setLayout(download_dataLayout);
        download_dataLayout.setHorizontalGroup(
            download_dataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(download_dataLayout.createSequentialGroup()
                .addGroup(download_dataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(download_dataLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(body_download, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(download_dataLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButton30, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(download_dataLayout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(header_download, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(248, Short.MAX_VALUE))
        );
        download_dataLayout.setVerticalGroup(
            download_dataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(download_dataLayout.createSequentialGroup()
                .addComponent(header_download, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(body_download, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton30)
                .addContainerGap(52, Short.MAX_VALUE))
        );

        body_masuk.add(download_data, "download_data");

        display_detail1.setBackground(new java.awt.Color(255, 255, 255));

        header_detail1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel101.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel101.setForeground(new java.awt.Color(0, 153, 204));
        jLabel101.setText("Detail Proyek Akhir");

        javax.swing.GroupLayout header_detail1Layout = new javax.swing.GroupLayout(header_detail1);
        header_detail1.setLayout(header_detail1Layout);
        header_detail1Layout.setHorizontalGroup(
            header_detail1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(header_detail1Layout.createSequentialGroup()
                .addGap(160, 160, 160)
                .addComponent(jLabel101)
                .addContainerGap(563, Short.MAX_VALUE))
        );
        header_detail1Layout.setVerticalGroup(
            header_detail1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel101)
        );

        body_detail1.setBackground(new java.awt.Color(255, 255, 255));
        body_detail1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));
        body_detail1.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N

        jLabel102.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel102.setText("Nama :");

        jLabel103.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel103.setText("NIM :");

        jLabel104.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel104.setText("Pembimbing :");

        jTextField18.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N

        jTextField19.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N

        jTextField20.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N

        jLabel105.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel105.setText("Abstraksi :");

        jTextArea3.setColumns(20);
        jTextArea3.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jTextArea3.setLineWrap(true);
        jTextArea3.setRows(5);
        jScrollPane9.setViewportView(jTextArea3);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));

        jButton36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/check.png"))); // NOI18N
        jButton36.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton36ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton36, javax.swing.GroupLayout.PREFERRED_SIZE, 68, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jButton36, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 34, Short.MAX_VALUE)
        );

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));

        jLabel106.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jLabel106.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel106.setText("?");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(53, 53, 53)
                .addComponent(jLabel106)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel106, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        javax.swing.GroupLayout body_detail1Layout = new javax.swing.GroupLayout(body_detail1);
        body_detail1.setLayout(body_detail1Layout);
        body_detail1Layout.setHorizontalGroup(
            body_detail1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, body_detail1Layout.createSequentialGroup()
                .addContainerGap(93, Short.MAX_VALUE)
                .addGroup(body_detail1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(body_detail1Layout.createSequentialGroup()
                        .addGroup(body_detail1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(body_detail1Layout.createSequentialGroup()
                                .addGroup(body_detail1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel105)
                                    .addComponent(jLabel103))
                                .addGap(37, 37, 37))
                            .addGroup(body_detail1Layout.createSequentialGroup()
                                .addComponent(jLabel104)
                                .addGap(18, 18, 18))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, body_detail1Layout.createSequentialGroup()
                                .addComponent(jLabel102)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(body_detail1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(body_detail1Layout.createSequentialGroup()
                                .addComponent(jTextField20, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(body_detail1Layout.createSequentialGroup()
                                .addGroup(body_detail1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jScrollPane9)
                                    .addComponent(jTextField18)
                                    .addComponent(jTextField19, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 43, Short.MAX_VALUE)
                                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, body_detail1Layout.createSequentialGroup()
                        .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(47, 47, 47))))
        );
        body_detail1Layout.setVerticalGroup(
            body_detail1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(body_detail1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(body_detail1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(body_detail1Layout.createSequentialGroup()
                        .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(body_detail1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel102))
                        .addGap(18, 18, 18)
                        .addGroup(body_detail1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel103))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(body_detail1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel104))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(body_detail1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel105)
                            .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        footer_detail1.setBackground(new java.awt.Color(255, 255, 255));

        jButton37.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/go-back-icon.png"))); // NOI18N
        jButton37.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton37ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout footer_detail1Layout = new javax.swing.GroupLayout(footer_detail1);
        footer_detail1.setLayout(footer_detail1Layout);
        footer_detail1Layout.setHorizontalGroup(
            footer_detail1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, footer_detail1Layout.createSequentialGroup()
                .addContainerGap(29, Short.MAX_VALUE)
                .addComponent(jButton37, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        footer_detail1Layout.setVerticalGroup(
            footer_detail1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jButton37, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        javax.swing.GroupLayout display_detail1Layout = new javax.swing.GroupLayout(display_detail1);
        display_detail1.setLayout(display_detail1Layout);
        display_detail1Layout.setHorizontalGroup(
            display_detail1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(display_detail1Layout.createSequentialGroup()
                .addComponent(header_detail1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 184, Short.MAX_VALUE))
            .addGroup(display_detail1Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(footer_detail1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(body_detail1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        display_detail1Layout.setVerticalGroup(
            display_detail1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(display_detail1Layout.createSequentialGroup()
                .addComponent(header_detail1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(display_detail1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(body_detail1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(footer_detail1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(18, Short.MAX_VALUE))
        );

        body_masuk.add(display_detail1, "detail");

        javax.swing.GroupLayout masuk_adminLayout = new javax.swing.GroupLayout(masuk_admin);
        masuk_admin.setLayout(masuk_adminLayout);
        masuk_adminLayout.setHorizontalGroup(
            masuk_adminLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(masuk_adminLayout.createSequentialGroup()
                .addGap(100, 100, 100)
                .addGroup(masuk_adminLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(body_masuk, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(title_masuk, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        masuk_adminLayout.setVerticalGroup(
            masuk_adminLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(masuk_adminLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(title_masuk, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(body_masuk, javax.swing.GroupLayout.PREFERRED_SIZE, 327, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        Admin.add(masuk_admin, "masuk_admin");

        Body.add(Admin, "admin");

        display_detail.setBackground(new java.awt.Color(255, 255, 255));
        display_detail.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));

        header_detail.setBackground(new java.awt.Color(255, 255, 255));

        jLabel87.setFont(new java.awt.Font("Calibri", 3, 24)); // NOI18N
        jLabel87.setForeground(new java.awt.Color(0, 153, 204));
        jLabel87.setText("Detail Proyek Akhir");

        javax.swing.GroupLayout header_detailLayout = new javax.swing.GroupLayout(header_detail);
        header_detail.setLayout(header_detailLayout);
        header_detailLayout.setHorizontalGroup(
            header_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(header_detailLayout.createSequentialGroup()
                .addGap(297, 297, 297)
                .addComponent(jLabel87)
                .addContainerGap(426, Short.MAX_VALUE))
        );
        header_detailLayout.setVerticalGroup(
            header_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(header_detailLayout.createSequentialGroup()
                .addComponent(jLabel87)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        body_detail.setBackground(new java.awt.Color(255, 255, 255));
        body_detail.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 153, 204), 1, true));
        body_detail.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N

        jLabel88.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel88.setText("Nama :");

        jLabel89.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel89.setText("NIM :");

        jLabel90.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel90.setText("Pembimbing :");

        jTextField15.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N

        jTextField16.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N

        jTextField17.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N

        jLabel92.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel92.setText("Abstraksi :");

        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jTextArea1.setLineWrap(true);
        jTextArea1.setRows(5);
        jScrollPane7.setViewportView(jTextArea1);

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));

        jLabel91.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jLabel91.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel91.setText("?");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(53, 53, 53)
                .addComponent(jLabel91)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel91, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        javax.swing.GroupLayout body_detailLayout = new javax.swing.GroupLayout(body_detail);
        body_detail.setLayout(body_detailLayout);
        body_detailLayout.setHorizontalGroup(
            body_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, body_detailLayout.createSequentialGroup()
                .addContainerGap(93, Short.MAX_VALUE)
                .addGroup(body_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(body_detailLayout.createSequentialGroup()
                        .addGroup(body_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(body_detailLayout.createSequentialGroup()
                                .addGroup(body_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel92)
                                    .addComponent(jLabel89))
                                .addGap(37, 37, 37))
                            .addGroup(body_detailLayout.createSequentialGroup()
                                .addComponent(jLabel90)
                                .addGap(18, 18, 18))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, body_detailLayout.createSequentialGroup()
                                .addComponent(jLabel88)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(body_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField17, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(body_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jScrollPane7)
                                .addComponent(jTextField15)
                                .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(138, 138, 138)))
                .addGap(47, 47, 47))
        );
        body_detailLayout.setVerticalGroup(
            body_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(body_detailLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(body_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel88))
                .addGap(18, 18, 18)
                .addGroup(body_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel89))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(body_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel90))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(body_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel92)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(34, Short.MAX_VALUE))
        );

        footer_detail.setBackground(new java.awt.Color(255, 255, 255));

        jButton32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/go-back-icon.png"))); // NOI18N
        jButton32.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton32ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout footer_detailLayout = new javax.swing.GroupLayout(footer_detail);
        footer_detail.setLayout(footer_detailLayout);
        footer_detailLayout.setHorizontalGroup(
            footer_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(footer_detailLayout.createSequentialGroup()
                .addComponent(jButton32, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 139, Short.MAX_VALUE))
        );
        footer_detailLayout.setVerticalGroup(
            footer_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jButton32, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        javax.swing.GroupLayout display_detailLayout = new javax.swing.GroupLayout(display_detail);
        display_detail.setLayout(display_detailLayout);
        display_detailLayout.setHorizontalGroup(
            display_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(display_detailLayout.createSequentialGroup()
                .addComponent(header_detail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 182, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, display_detailLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(display_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(footer_detail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(body_detail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(226, 226, 226))
        );
        display_detailLayout.setVerticalGroup(
            display_detailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(display_detailLayout.createSequentialGroup()
                .addComponent(header_detail, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(body_detail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(footer_detail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(23, Short.MAX_VALUE))
        );

        Body.add(display_detail, "detail");

        footer.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        footer.setText("@Copyright - TeamLucu");

        javax.swing.GroupLayout homeLayout = new javax.swing.GroupLayout(home);
        home.setLayout(homeLayout);
        homeLayout.setHorizontalGroup(
            homeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(homeLayout.createSequentialGroup()
                .addGroup(homeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(homeLayout.createSequentialGroup()
                        .addGap(492, 492, 492)
                        .addComponent(footer))
                    .addGroup(homeLayout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addComponent(header, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(12, Short.MAX_VALUE))
            .addGroup(homeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, homeLayout.createSequentialGroup()
                    .addContainerGap(28, Short.MAX_VALUE)
                    .addComponent(Body, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(29, Short.MAX_VALUE)))
        );
        homeLayout.setVerticalGroup(
            homeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(homeLayout.createSequentialGroup()
                .addGap(60, 60, 60)
                .addComponent(header, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 424, Short.MAX_VALUE)
                .addComponent(footer)
                .addGap(139, 139, 139))
            .addGroup(homeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, homeLayout.createSequentialGroup()
                    .addContainerGap(219, Short.MAX_VALUE)
                    .addComponent(Body, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(173, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout utamaLayout = new javax.swing.GroupLayout(utama);
        utama.setLayout(utamaLayout);
        utamaLayout.setHorizontalGroup(
            utamaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(utamaLayout.createSequentialGroup()
                .addComponent(home, javax.swing.GroupLayout.PREFERRED_SIZE, 1159, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 1, Short.MAX_VALUE))
        );
        utamaLayout.setVerticalGroup(
            utamaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(home, javax.swing.GroupLayout.PREFERRED_SIZE, 825, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/background.jpg"))); // NOI18N

        javax.swing.GroupLayout backgroundLayout = new javax.swing.GroupLayout(background);
        background.setLayout(backgroundLayout);
        backgroundLayout.setHorizontalGroup(
            backgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(backgroundLayout.createSequentialGroup()
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 1439, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 30, Short.MAX_VALUE))
        );
        backgroundLayout.setVerticalGroup(
            backgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, backgroundLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 836, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout backLayout = new javax.swing.GroupLayout(back);
        back.setLayout(backLayout);
        backLayout.setHorizontalGroup(
            backLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(backLayout.createSequentialGroup()
                .addGap(102, 102, 102)
                .addComponent(utama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(backLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(backLayout.createSequentialGroup()
                    .addComponent(background, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        backLayout.setVerticalGroup(
            backLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(backLayout.createSequentialGroup()
                .addComponent(utama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(backLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(backLayout.createSequentialGroup()
                    .addComponent(background, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        jMenuBar1.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N

        file.setText("File");
        file.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N

        jMenuItem6.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem6.setText("New Windows");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        file.add(jMenuItem6);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        jMenuItem2.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jMenuItem2.setText("Exit");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        file.add(jMenuItem2);

        jMenuBar1.add(file);

        edit.setText("Edit");
        edit.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N

        jMenuItem7.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Y, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem7.setText("Redo");
        edit.add(jMenuItem7);

        jMenuItem8.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem8.setText("Undo");
        edit.add(jMenuItem8);

        jMenuItem5.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem5.setText("Cut");
        edit.add(jMenuItem5);

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem3.setText("Copy");
        edit.add(jMenuItem3);

        jMenuItem4.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem4.setText("Paste");
        edit.add(jMenuItem4);

        jMenuBar1.add(edit);

        help.setText("Help");
        help.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem1.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jMenuItem1.setText("Help");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        help.add(jMenuItem1);

        About.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        About.setText("About");
        About.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AboutActionPerformed(evt);
            }
        });
        help.add(About);

        jMenuBar1.add(help);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(back, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(back, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        CardLayout cl = (CardLayout)Body.getLayout();
        cl.show(Body, "login");
        jLabel8.setVisible(false);
        jLabel9.setVisible(false);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:

        CardLayout cl = (CardLayout) body_registrasi.getLayout();
        cl.show(body_registrasi, "page2");
        jButton9.setVisible(false);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) body_registrasi.getLayout();
        cl.show(body_registrasi, "page1");
        jButton3.setEnabled(false);
        jCheckBox1.setSelected(false);
        jButton9.setVisible(true);
    }//GEN-LAST:event_jButton6ActionPerformed
    
    public void getNIP(JComboBox box) {
        Database db = new Database();
        try {
            ResultSet rs = db.getResult("select nip from pembimbing where nama_pembimbing='" + box.getSelectedItem().toString() + "'");
            while (rs.next()) {
                nip = rs.getString("nip");
            }
            rs.close();
        } catch (Throwable f) {
            JOptionPane.showMessageDialog(null, "" + f.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }


    }

    public void getID_fakultas(JComboBox c) {
        if (c.getSelectedIndex() == 0) {
            id_fak = "FAK-01";
        } else if (c.getSelectedIndex() == 1) {
            id_fak = "FAK-02";
        }
    }

    public String insert(String tabel) {
        String ID_PA = null;
        getID_fakultas(fakultas);
        getNIP(pembimbing);

        return "insert into " + tabel + " values('" + nim.getText() + "','" + nama.getText() + "','" + tahun.getSelectedItem().toString() + "','"
                + no_hp.getText() + "','" + no_telp.getText() + "','" + alamat.getText() + "','" + password.getText() + "','"
                + jDateChooser1.getDate().getYear() + "-" + (jDateChooser1.getDate().getMonth() + 1) + "-" + jDateChooser1.getDate().getDate() + "','"
                + email.getText() + "','" + nip + "','" + id_fak + "')";
    }

    public int getMaks()
    {
        int maks = 0;
        Database db = new Database();
        ResultSet rs = null;
        try
        {
            rs = db.getResult("select id_pa from pa");
            while (rs.next())
            {
                if (maks < Integer.parseInt(rs.getString("id_pa")))
                {
                    maks = Integer.parseInt(rs.getString("id_pa"));
                }
            }
            rs.close();
        }
        catch (Throwable f)
        {
            JOptionPane.showMessageDialog(null, ""+f.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
        return maks;
    }
    public String generateIdPA() throws SQLException {
        Database db = new Database();
        ResultSet rs = null;
        String query = "select id_pa from PA order by id_pa ASC";
        rs = db.getResult(query);
        String ID_PA="";
        if(rs.last()){
            int ID = getMaks();
            ID_PA = ""+(++ID);
        }else{
            ID_PA="1";
        }
        return ID_PA;
    }

    public boolean cekAngka(JTextField text) {
        boolean benar = true;
        try {
            double i = Double.parseDouble(text.getText());

        } catch (Throwable f) {
            benar = false;
        }

        return benar;

    }
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        Database db = new Database();
        int i = 0;
        if (ulang_password.getText().equals(password.getText())) {
            if (password.getText().length() >= 6) {
                if (!cekAngka(nama)) {
                    if (cekAngka(nim)) {
                        if (no_hp.getText().equals("") && !no_telp.getText().equals("")) {
                            if (cekAngka(no_telp)) {
                                if (Double.parseDouble(no_telp.getText()) % 2 != 0) {
                                    int a = JOptionPane.showConfirmDialog(null, "Anda Yakin ?", "Konfirmasi", JOptionPane.OK_CANCEL_OPTION);
                                    if (a == 0) {
                                        db.query(insert("mahasiswa"));
                                        if (db.isInsert) {

                                            JOptionPane.showMessageDialog(null, "Pendaftaran Berhasil");
                                            CardLayout cl = (CardLayout) Body.getLayout();
                                            cl.show(Body, "depan");
                                            jLabel8.setVisible(true);
                                            jLabel9.setVisible(true);
                                            reset();
                                            
                                            
                                        }
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(null, "Maaf, nomer telpon tidak valid", "Error", JOptionPane.ERROR_MESSAGE);

                                }
                            } else {
                                JOptionPane.showMessageDialog(null, "Maaf, nomor telepon tidak valid", "Error", JOptionPane.ERROR_MESSAGE);
                            }

                        } else if (!no_hp.getText().equals("") && no_telp.getText().equals("")) {
                            if (cekAngka(no_hp)) {
                                if (Double.parseDouble(no_hp.getText()) % 2 != 0) {
                                    int a = JOptionPane.showConfirmDialog(null, "Anda Yakin ?", "Konfirmasi", JOptionPane.OK_CANCEL_OPTION);
                                    if (a == 0) {
                                        db.query(insert("mahasiswa"));
                                        if (db.isInsert) {

                                            JOptionPane.showMessageDialog(null, "Pendaftaran Berhasil");
                                            CardLayout cl = (CardLayout) Body.getLayout();
                                            cl.show(Body, "depan");
                                            jLabel8.setVisible(true);
                                            jLabel9.setVisible(true);
                                            reset();
                                            update("No", i, jTable1);
                                        }
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(null, "Maaf, nomor telepon tidak valid", "Error", JOptionPane.ERROR_MESSAGE);
                                }
                            } else {
                                JOptionPane.showMessageDialog(null, "Maaf, nomor telepon tidak valid", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        } else if (!no_hp.getText().equals("") && !no_telp.getText().equals("")) {
                            if (cekAngka(no_hp) && cekAngka(no_telp)) {
                                if (Double.parseDouble(no_hp.getText()) % 2 != 0 && Double.parseDouble(no_telp.getText()) % 2 != 0) {
                                    int a = JOptionPane.showConfirmDialog(null, "Anda Yakin ?", "Konfirmasi", JOptionPane.OK_CANCEL_OPTION);
                                    if (a == 0) {
                                        db.query(insert("mahasiswa"));
                                        if (db.isInsert) {

                                            JOptionPane.showMessageDialog(null, "Pendaftaran Berhasil");
                                            CardLayout cl = (CardLayout) Body.getLayout();
                                            cl.show(Body, "depan");
                                            jLabel8.setVisible(true);
                                            jLabel9.setVisible(true);
                                            reset();
                                            update("No", i, jTable1);
                                        }
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(null, "Maaf, nomor telepon tidak valid", "Error", JOptionPane.ERROR_MESSAGE);
                                }
                            } else {
                                JOptionPane.showMessageDialog(null, "Maaf, nomor telepon tidak valid", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        } else {
                            JOptionPane.showMessageDialog(null, "Maaf, nomor telepon harus di isi", "Error", JOptionPane.ERROR_MESSAGE);
                        }



                    } else {
                        JOptionPane.showMessageDialog(null, "Maaf, NIM tidak valid", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Maaf, nama tidak valid", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Maaf, password kurang dari 6 karakter", "Error", JOptionPane.ERROR_MESSAGE);
            }

        } else {
            JOptionPane.showMessageDialog(null, "Maaf, konfirmasi password tidak cocok", "Error", JOptionPane.ERROR_MESSAGE);
        }




    }//GEN-LAST:event_jButton3ActionPerformed

    public void reset() {
        nama.setText("");
        nim.setText("");
        no_hp.setText("");
        no_telp.setText("");
        email.setText("");
        alamat.setText("");
        fakultas.setSelectedIndex(0);
        jDateChooser1.setDate(null);
        password.setText("");
        ulang_password.setText("");
        tahun.setSelectedIndex(0);
        pembimbing.setSelectedIndex(0);
        
        jCheckBox1.setSelected(false);
        jButton3.setEnabled(false);
        nama1.setEnabled(false);
        nim1.setEnabled(false);
        no_hp1.setEnabled(false);
        no_telp1.setEnabled(false);
        email1.setEnabled(false);
        alamat1.setEnabled(false);
        fakultas1.setEnabled(false);
        jButton5.setEnabled(false);


    }

    private void jLabel19MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel19MouseClicked
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) Body.getLayout();
        cl.show(Body, "registrasi");
        CardLayout cl1 = (CardLayout) body_registrasi.getLayout();
        cl1.show(body_registrasi, "page1");
    }//GEN-LAST:event_jLabel19MouseClicked

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void jLabel25MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel25MouseClicked
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) Body.getLayout();
        cl.show(Body, "lupas_pass");
    }//GEN-LAST:event_jLabel25MouseClicked

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        Database db = new Database();
        boolean isAda = false;
        String pass = null;
        ResultSet rs = null;
        try {

            if (!isAdmin) {
                //select password from admin where email='admin@yahoo.com';
                rs = db.getResult("select * from mahasiswa where email='" + jTextField8.getText() + "'");
                while (rs.next()) {
                    if (rs.getString("email").equals(jTextField8.getText())) {
                        pass = rs.getString("password");
                        isAda = true;
                    }
                }
                if (isAda) {
                    JOptionPane.showMessageDialog(null, "Password anda : " + pass);
                } else {
                    JOptionPane.showMessageDialog(null, "Maaf, email tidak terdaftar ", "Error", JOptionPane.ERROR_MESSAGE);
                }


                CardLayout cl = (CardLayout) Body.getLayout();
                cl.show(Body, "login");
            } else {
                rs = db.getResult("select * from admin where email='" + jTextField8.getText() + "'");
                while (rs.next()) {
                    if (rs.getString("email").equals(jTextField8.getText())) {
                        pass = rs.getString("password");
                        isAda = true;
                    }
                }
                if (isAda) {
                    JOptionPane.showMessageDialog(null, "Password anda : " + pass);
                } else {
                    JOptionPane.showMessageDialog(null, "Maaf, email tidak terdaftar ", "Error", JOptionPane.ERROR_MESSAGE);
                }

                CardLayout cl1 = (CardLayout) Admin.getLayout();
                cl1.show(Admin, "login_admin");
            }
            rs.close();
        } catch (SQLException | HeadlessException f) {
            JOptionPane.showMessageDialog(null, "" + f.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }

        jTextField8.setText("");
        jButton7.setEnabled(false);


    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) Body.getLayout();
        cl.show(Body, "login");
        reset();
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) Body.getLayout();
        cl.show(Body, "depan");
        jLabel8.setVisible(true);
        jLabel9.setVisible(true);
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        // TODO add your handling code here:
        if (!isAdmin) {
            CardLayout cl = (CardLayout) Body.getLayout();
            cl.show(Body, "login");
        } else {
            CardLayout cl = (CardLayout) Body.getLayout();
            cl.show(Body, "admin");
        }
    }//GEN-LAST:event_jButton10ActionPerformed

    public String cari(String tabel, String clause) {
        return "select nim, password from " + tabel + " where nim='" + clause + "'";
    }

    public String cari(String tabel, String where, String clause) {
        return "select nim, password from " + tabel + " where " + where + "='" + clause + "'";
    }

    public String cari(String dis1, String tabel, String where, String clause) {
        return "select " + dis1 + " from " + tabel + " where " + where + "='" + clause + "'";
    }

    public String cari(String dis1) {
        return "select " + dis1 + " from admin";
    }

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        Database db = new Database();

        boolean masuk = false;
        String judul = null;
        String nama = null;
        ResultSet rs = null;
        try {
            rs = db.getResult("select nim, password, nama_mahasiswa from mahasiswa where nim='" + jTextField1.getText() + "' AND password='"+jPasswordField1.getText()+"'");
            while (rs.next()) {
                if (!rs.getString("password").equals(jPasswordField1.getText()) && !rs.getString("nim").equals(jTextField1.getText())) {

                    masuk = false;
                } else {
                   
                    nama = rs.getString("nama_mahasiswa");
                    temp = rs.getString("nim");
                    
                    masuk = true;

                }
            }
            if (masuk) {
                CardLayout cl1 = (CardLayout) Login.getLayout();
                cl1.show(Login, "menu_mhs");
                CardLayout cl = (CardLayout) body_mhs.getLayout();
                cl.show(body_mhs, "menu_utama");
                
                jLabel6.setText("[" + nama + "] masuk sebagai mahasiswa");

            } else {
                JOptionPane.showMessageDialog(null, "Maaf, NIM dan password salah", "Error", JOptionPane.ERROR_MESSAGE);
                i--;
                if (i == 0) {
                    CardLayout cl2 = (CardLayout) Body.getLayout();
                    cl2.show(Body, "depan");
                }
            }
            rs.close();
        } catch (SQLException | HeadlessException f) {
            JOptionPane.showMessageDialog(null, "" + f.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
        jTextField1.setText("");
        jPasswordField1.setText("");


    }//GEN-LAST:event_jButton4ActionPerformed

    public void upload() {
        CardLayout cl = (CardLayout) body_mhs.getLayout();
        cl.show(body_mhs, "upload");

    }
    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        // TODO add your handling code here:
        //Mengambil ID Fak dari Database berdasarkan NIM
        Database db = new Database();
        ResultSet rs = null;
        try {
            rs = db.getResult("select id_fakultas from mahasiswa where nim='" + temp + "'");
            while (rs.next()) {
                id_fak = rs.getString("id_fakultas");
            }
            rs.close();
        } catch (Throwable f) {
        }
        System.out.println("" + id_fak);
        String cekPDF = "fdp";
        URL = "D:/[Kuliah]Semester 3/PBO/DATA TUBES/Aplikasi Pengelolaan PA/Upload/";
        if (id_fak.equalsIgnoreCase("Fak-01")) {
            URL += "IF/";
        } else {
            URL += "TT/";
        }
        URL += temp + "/";

        File f1 = new File(URL);
        Boolean status;
        try {
            if (f1.mkdir()) {
                //JOptionPane.showMessageDialog(null, "Folder berhasil di buat");
            }

        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Gagal");
        }
        JFileChooser file = new JFileChooser();

        file.setDialogTitle("Pilih file");
        FileNameExtensionFilter PDFFilter = new FileNameExtensionFilter("PDF File", "pdf");
        file.setFileFilter(PDFFilter);
        int returnFile = file.showSaveDialog(this);
        if (returnFile == 0) {
            if (cekPDF.equals(fileExtension_picker(file))) {
                URLAsal = file.getSelectedFile().getPath();
                //memasukkan nama file
                URL += temp + ".pdf";
                
                
                jLabel107.setText(file.getSelectedFile().getName());
               isi3=true;
                if (jCheckBox2.isSelected()) {
                    jButton33.setEnabled(true);
                } else {
                    jButton33.setEnabled(false);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Maaf, file bukan PDF", "Error", JOptionPane.ERROR_MESSAGE);
               isi3=false;
                if (jCheckBox2.isSelected()) {
                    jButton33.setEnabled(true);
                } else {
                    jButton33.setEnabled(false);
                }


            }
        }

    }//GEN-LAST:event_jButton11ActionPerformed

    private void upload(String fromFileURL, String toFileURL) throws IOException {
        FileChannel fromFile = null;
        FileChannel toFile = null;
        try {
            fromFile = new FileInputStream(fromFileURL).getChannel();
            toFile = new FileOutputStream(toFileURL).getChannel();
            toFile.transferFrom(fromFile, 0, fromFile.size());
        } catch(Throwable f)
        {
            f.printStackTrace();
        }
            finally {
            fromFile.close();
            toFile.close();
        }
    }

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) Body.getLayout();
        cl.show(Body, "cari");
        jLabel8.setVisible(false);
        jLabel9.setVisible(false);
        isCari = true;
        String[] name = {"Nama Mahasiswa", "NIM", "Judul PA", "Nama Pembimbing", "Fakultas"};
        DefaultTableModel tb = new DefaultTableModel(name, 0);
        jTable2.setModel(tb);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jComboBox6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox6ActionPerformed
        // TODO add your handling code here:

        jLabel36.setText("Masukkan " + jComboBox6.getSelectedItem().toString() + ": ");
        jTextField9.setText("");
        jComboBox5.setEnabled(false);

    }//GEN-LAST:event_jComboBox6ActionPerformed

    private void jButton13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton13ActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) Body.getLayout();
        cl.show(Body, "depan");
        jLabel8.setVisible(true);
        jLabel9.setVisible(true);
        jComboBox5.setSelectedIndex(0);
        jComboBox6.setSelectedIndex(0);
        jTextField9.setText("");
        String[] name = {"ID PA", "Judul PA", "NIM", "Nama Mahasiswa", "Fakultas"};
        DefaultTableModel tb = new DefaultTableModel(name, 0);
        jTable2.setModel(tb);
        isCari = false;
        jComboBox5.setEnabled(false);
    }//GEN-LAST:event_jButton13ActionPerformed

    private void jButton15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton15ActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) Body.getLayout();
        cl.show(Body, "depan");
        int i = 0;
        jLabel8.setVisible(true);
        update("No", i, jTable1);
        jLabel9.setVisible(true);
        this.isAdmin = false;

       

        Database db = new Database();
        try {
            ResultSet rs = db.getResult("select * from pembimbing");
            while (rs.next()) {
                pembimbing.addItem(rs.getString("nama_pembimbing"));
                pembimbing1.addItem(rs.getString("nama_pembimbing"));
            }
        } catch (Throwable f) {
            JOptionPane.showMessageDialog(null, "" + f.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton15ActionPerformed

    private void jButton14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton14ActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) Body.getLayout();
        cl.show(Body, "admin");
        this.isAdmin = true;

        




    }//GEN-LAST:event_jButton14ActionPerformed

    private void jTextField10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField10ActionPerformed

    public String execute(String tabel, String sql, String nama) {
        String s = "select * from " + tabel + " where " + sql + "='" + nama + "'";
        return s;
    }

    private void jButton16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton16ActionPerformed
        // TODO add your handling code here:
        boolean masuk = false;
        Database db = new Database();
        try {
            ResultSet rs = db.getResult("select username,password from admin where username='" + jTextField10.getText() + "'");
            while (rs.next()) {
                if (rs.getString("username").equals(jTextField10.getText()) && rs.getString("password").equals(jPasswordField4.getText())) {
                    masuk = true;
                } else {
                    masuk = false;
                }
            }
            if (masuk) {
                CardLayout cl = (CardLayout) Admin.getLayout();
                cl.show(Admin, "masuk_admin");
                CardLayout cl1 = (CardLayout) body_masuk.getLayout();
                cl1.show(body_masuk, "utama_admin");
            } else {
                JOptionPane.showMessageDialog(null, "Username dan password tidak cocok", "Error", JOptionPane.ERROR_MESSAGE);
                j--;
                if (j == 0) {
                    CardLayout cl1 = (CardLayout) Body.getLayout();
                    cl1.show(Body, "pilih_user");
                }
            }

            rs.close();
        } catch (SQLException | HeadlessException f) {
            JOptionPane.showMessageDialog(null, "" + f.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }

        jTextField10.setText("");
        jPasswordField4.setText("");
    }//GEN-LAST:event_jButton16ActionPerformed

    private void jLabel41MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel41MouseClicked
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) Body.getLayout();
        cl.show(Body, "lupas_pass");
    }//GEN-LAST:event_jLabel41MouseClicked

    private void jButton17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton17ActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) Body.getLayout();
        cl.show(Body, "pilih_user");

    }//GEN-LAST:event_jButton17ActionPerformed

    private void jCheckBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox2ActionPerformed
        // TODO add your handling code here:
        if (jCheckBox2.isSelected()) {
            ischeck = true;
        } else {
            ischeck = false;
        }

        if (ischeck && isi3) {

            jButton33.setEnabled(true);
        } else {
            jButton33.setEnabled(false);

        }
    }//GEN-LAST:event_jCheckBox2ActionPerformed

    private void jButton19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton19ActionPerformed
        // TODO add your handling code here:
        //maintanance();
        ada = false;
        Database db = new Database();
        ResultSet rs = null;
        try {
            rs = db.getResult("select url from pa where nim='" + temp + "'");
            while (rs.next()) {
                if (!rs.getString("url").equals("")) {
                    ada = true;
                } else {
                    ada = false;
                }
            }
            rs.close();
            if (ada) {
                int a = JOptionPane.showConfirmDialog(null, "Anda sudah upload file. Anda yakin upload lagi ?", "Konfirmasi", JOptionPane.OK_CANCEL_OPTION);
                if (a == 0) {
                    upload();
                }
            } else {
                upload();
            }
        } catch (SQLException f) {
            JOptionPane.showMessageDialog(null, "Terjadi kesalahan dalam database", "Error", JOptionPane.ERROR_MESSAGE);
        }
        isi = false;
        isi1 = false;

    }//GEN-LAST:event_jButton19ActionPerformed

    private void jButton21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton21ActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) Login.getLayout();
        cl.show(Login, "awal");
        jCheckBox2.setSelected(false);
        jButton11.setEnabled(false);
    }//GEN-LAST:event_jButton21ActionPerformed

    private void jButton18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton18ActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) Admin.getLayout();
        cl.show(Admin, "login_admin");
    }//GEN-LAST:event_jButton18ActionPerformed

    private void jTextField8KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField8KeyTyped
        // TODO add your handling code here:
        if (!jTextField8.getText().equals("")) {
            jButton7.setEnabled(true);
        } else {
            jButton7.setEnabled(false);
        }
    }//GEN-LAST:event_jTextField8KeyTyped

    private void jButton22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton22ActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) body_mhs.getLayout();
        cl.show(body_mhs, "menu_utama");
        jTextArea2.setText("");
        jCheckBox2.setSelected(false);
        jButton11.setEnabled(false);
        isi = false;
        jLabel107.setText("?");
        jTextField21.setText("");
        isi3 = false;
    }//GEN-LAST:event_jButton22ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void AboutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AboutActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "APPA (Aplikasi Pengelolaan Proyek Akhir).\n"
                + "By Team Lucu.\n"
                + "Coder : Muhammad Fauzan.\n"
                + "Designer : Nanda Hasanal Rizky.\n"
                + "System Analyst : Laura Sinar Utami.");
    }//GEN-LAST:event_AboutActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        // TODO add your handling code here:
        new Screen().setVisible(true);
        new Screen().setTitle("Aplikasi Pengelolaan Proyek Akhir");
        new Screen().setLocationRelativeTo(null);
        new Screen().setResizable(false);


        //[1355, 750]
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    public String fileExtension_picker(JFileChooser file) {

        int n = file.getSelectedFile().getPath().length();
        return "" + file.getSelectedFile().getPath().charAt(n - 1) + "" + file.getSelectedFile().getPath().charAt(n - 2) + "" + file.getSelectedFile().getPath().charAt(n - 3);
    }

    public void edit_akun() {

        Database db = new Database();
        ResultSet rs = null;
        try {
            rs = db.getResult("select nama_mahasiswa, nim, no_hp, no_telp, email, alamat, nama_fakultas, tgl_lahir, password, tahun, nama_pembimbing from mahasiswa join fakultas using(id_fakultas) join pembimbing using(nip) where nim= '" + temp + "'");
            while (rs.next()) {
                 
                 
                nama1.setText(rs.getString("nama_mahasiswa"));
                nim1.setText(rs.getString("nim"));
                no_hp1.setText(rs.getString("no_hp"));
                no_telp1.setText(rs.getString("no_telp"));
                email1.setText(rs.getString("email"));
                alamat1.setText(rs.getString("alamat"));
                fakultas1.setSelectedItem(rs.getString("nama_fakultas"));
                jDateChooser2.setDate(Date.valueOf(rs.getString("tgl_lahir")));
                jYearChooser1.setYear(Date.valueOf(rs.getString("tgl_lahir")).getYear());
                password1.setText(rs.getString("password"));
                tahun1.setSelectedItem(rs.getString("tahun"));
                pembimbing1.setSelectedItem(rs.getString("nama_pembimbing"));
                
            }
            rs.close();
        } catch (SQLException f) {
            JOptionPane.showMessageDialog(null, "" + f.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
        CardLayout cl = (CardLayout) body_mhs.getLayout();
        cl.show(body_mhs, "edit_akun");
    }

    public void maintanance() {
        JOptionPane.showMessageDialog(null, "Sistem baru di perbaiki. Mohon bersabar", "WARNING", JOptionPane.WARNING_MESSAGE);
    }

    private void jButton20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton20ActionPerformed
        // TODO add your handling code here:
        edit_akun();
        CardLayout cl = (CardLayout) body_edit.getLayout();
        cl.show(body_edit, "page1");
        nama1.setEnabled(true);
        no_hp1.setEnabled(true);
        no_telp1.setEnabled(true);
        email1.setEnabled(true);
        alamat1.setEnabled(true);
        fakultas1.setEnabled(true);






    }//GEN-LAST:event_jButton20ActionPerformed

    public void cariPA() {
        String[] name = {"Nama Mahasiswa", "NIM", "Judul PA", "Nama Pembimbing", "Fakultas"};
        DefaultTableModel tb = new DefaultTableModel(name, 0);
        Database db = new Database();
        ResultSet rs = null;
        boolean ketemu = false;
        switch (jComboBox6.getSelectedIndex()) {
            case 0: {
                try {
                    rs = db.getResult("select judul_pa, nim, nama_mahasiswa, nama_fakultas,nama_pembimbing from mahasiswa join pa using (nim) join fakultas using (id_fakultas) join pembimbing using(nip) where nim like '%" + jTextField9.getText() + "%'");
                    while (rs.next()) {
                        String[] data = {rs.getString("nama_mahasiswa"), rs.getString("nim"), rs.getString("judul_pa"), rs.getString("nama_pembimbing"), rs.getString("nama_fakultas")};
                        tb.addRow(data);
                    }

                    jTable2.setModel(tb);
                    rs.close();
                } catch (Throwable f) {
                    JOptionPane.showMessageDialog(null, "" + f.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
                break;
            }
            case 1: {
                try {
                    rs = db.getResult("select judul_pa, nim, nama_mahasiswa, nama_fakultas,nama_pembimbing from mahasiswa join pa using (nim) join fakultas using (id_fakultas) join pembimbing using(nip) where judul_pa like '%" + jTextField9.getText() + "%'");
                    while (rs.next()) {
                        String[] data = {rs.getString("nama_mahasiswa"), rs.getString("nim"), rs.getString("judul_pa"), rs.getString("nama_pembimbing"), rs.getString("nama_fakultas")};
                        tb.addRow(data);
                    }

                    jTable2.setModel(tb);
                    rs.close();
                } catch (Throwable f) {
                    JOptionPane.showMessageDialog(null, "" + f.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
                break;
            }
            case 2: {
                try {
                    rs = db.getResult("select judul_pa, nim, nama_mahasiswa, nama_fakultas,nama_pembimbing from mahasiswa join pa using (nim) join fakultas using (id_fakultas) join pembimbing using(nip) where nama_fakultas like '%" + jTextField9.getText() + "%'");
                    while (rs.next()) {
                        String[] data = {rs.getString("nama_mahasiswa"), rs.getString("nim"), rs.getString("judul_pa"), rs.getString("nama_pembimbing"), rs.getString("nama_fakultas")};
                        tb.addRow(data);
                    }

                    jTable2.setModel(tb);
                    rs.close();
                } catch (Throwable f) {
                    JOptionPane.showMessageDialog(null, "" + f.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
                break;
            }
        }

    }

    public void cariPA(String order, String by, JTable tabel) {
        //{no,"Nama Mahasiswa","NIM","Judul PA","Nama Pembimbing"};
        String[] name = {"Nama Mahasiswa", "NIM", "Judul PA", "Nama Pembimbing", "Fakultas"};
        DefaultTableModel tb = new DefaultTableModel(name, 0);
        Database db = new Database();
        ResultSet rs = null;
        switch (jComboBox6.getSelectedIndex()) {
            case 0: {
                try {
                    rs = db.getResult("select judul_pa, nim, nama_mahasiswa, nama_pembimbing,nama_fakultas from mahasiswa join pa using (nim) join pembimbing using(nip) join fakultas using(id_fakultas) where nim like '%" + jTextField9.getText() + "%' " + order + " by " + by);
                    while (rs.next()) {
                        String[] data = {rs.getString("nama_mahasiswa"), rs.getString("nim"), rs.getString("judul_pa"), rs.getString("nama_pembimbing"), rs.getString("nama_fakultas")};
                        tb.addRow(data);

                    }
                    tabel.setModel(tb);
                    rs.close();
                } catch (Throwable f) {
                    JOptionPane.showMessageDialog(null, "" + f.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
                break;
            }
            case 1: {
                try {
                    rs = db.getResult("select judul_pa, nim, nama_mahasiswa, nama_pembimbing,nama_fakultas from mahasiswa join pa using (nim) join pembimbing using(nip) join fakultas using(id_fakultas) where judul_pa like '%" + jTextField9.getText() + "%' " + order + " by " + by);
                    while (rs.next()) {
                        String[] data = {rs.getString("nama_mahasiswa"), rs.getString("nim"), rs.getString("judul_pa"), rs.getString("nama_pembimbing"), rs.getString("nama_fakultas")};
                        tb.addRow(data);

                    }
                    tabel.setModel(tb);
                    rs.close();
                } catch (Throwable f) {
                    JOptionPane.showMessageDialog(null, "" + f.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
                break;
            }
            case 2: {
                try {
                    rs = db.getResult("select judul_pa, nim, nama_mahasiswa, nama_pembimbing,nama_fakultas from mahasiswa join pa using (nim) join pembimbing using(nip) join fakultas using(id_fakultas) where nama_fakultas like '%" + jTextField9.getText() + "%' " + order + " by " + by);
                    while (rs.next()) {
                        String[] data = {rs.getString("nama_mahasiswa"), rs.getString("nim"), rs.getString("judul_pa"), rs.getString("nama_pembimbing"), rs.getString("nama_fakultas")};
                        tb.addRow(data);

                    }
                    tabel.setModel(tb);
                    rs.close();
                } catch (Throwable f) {
                    JOptionPane.showMessageDialog(null, "" + f.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
                break;
            }
        }
    }

    public void cariPA(String order, String by) {
        //{no,"Nama Mahasiswa","NIM","Judul PA","Nama Pembimbing"};
        String[] name = {"ID PA", "Nama Mahasiswa", "NIM", "Judul PA", "Nama Pembimbing", "Fakultas"};
        DefaultTableModel tb = new DefaultTableModel(name, 0);
        Database db = new Database();
        ResultSet rs = null;
        try {
            rs = db.getResult("select id_pa, judul_pa, nim, nama_mahasiswa, nama_pembimbing,nama_fakultas from mahasiswa join pa using (nim) join pembimbing using (nip) join fakultas using(id_fakultas) " + order + " by " + by);
            while (rs.next()) {
                String[] data = {rs.getString("id_pa"), rs.getString("nama_mahasiswa"), rs.getString("nim"), rs.getString("judul_pa"), rs.getString("nama_pembimbing"), rs.getString("nama_fakultas")};
                tb.addRow(data);

            }
            jTable3.setModel(tb);
            rs.close();
        } catch (Throwable f) {
            JOptionPane.showMessageDialog(null, "" + f.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
        // TODO add your handling code here:
        cariPA();
        jComboBox5.setEnabled(true);

    }//GEN-LAST:event_jButton12ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "APPA adalah aplikasi pengelolaan proyek akhir.\n"
                + "Aplikasi ini dapat digunakan oleh admin dan mahasiswa D3 IT Telkom.\n"
                + "Yang memiliki fungsionalitas seperti input file PA, cari file PA, menampilkan file PA.\n"
                + "Langkah-langkah penggunaan aplikasi : \n"
                + "1. Pilih user terlebih dahulu.\n"
                + "2. Jika anda masuk sebagai admin, anda harus login terlebih dahulu.\n"
                + "3. Jika anda masuk sebagai mahasiswa, anda dapat melihat file PA dan login bila anda ingin\n"
                + "   meng-upload file PA dan meng-edit akun anda.\n"
                + "4. Sebelum anda meng-upload file PA, anda harus menuliskan abstraksi dari PA anda.");
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jButton23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton23ActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) body_edit.getLayout();
        cl.show(body_edit, "page2");
        jButton29.setVisible(false);
    }//GEN-LAST:event_jButton23ActionPerformed

    public boolean cekAngka(JTextArea alamat1) {
        boolean benar = true;
        try {
            double i = Double.parseDouble(alamat1.getText());

        } catch (Throwable f) {
            benar = false;
        }

        return benar;
    }
    private void jButton24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton24ActionPerformed
        // TODO add your handling code here:
        getNIP(pembimbing1);
        getID_fakultas(fakultas1);
        int i = 0;
        Database db = new Database();

        if (password1.getText().equals(ulang_password1.getText())) {
            if (!cekAngka(nama1) && !nama1.getText().equals("")) {
                if (password1.getText().length() >= 6) {
                    if (!cekAngka(alamat1) && !alamat1.getText().equals("")) {
                        if (!email1.getText().equals("")) {
                            if (no_hp1.getText().equals("") && !no_telp1.getText().equals("")) {
                                if (cekAngka(no_telp1)) {
                                    db.query("update mahasiswa set nama_mahasiswa='" + nama1.getText() + "', nim='" + nim1.getText() + "',no_hp='" + no_hp1.getText() + "',no_telp='" + no_telp1.getText() + "', email='" + email1.getText() + "', alamat='" + alamat1.getText() + "', id_fakultas='" + id_fak + "', tgl_lahir='" + jDateChooser2.getDate().getYear() + "-" + (jDateChooser2.getDate().getMonth() + 1) + "-" + jDateChooser2.getDate().getDate() + "', password='" + password1.getText() + "', tahun='" + tahun1.getSelectedItem().toString() + "', nip='" + nip + "' where nim='" + temp + "'");
                                    if (db.isInsert) {
                                        JOptionPane.showMessageDialog(null, "Data berhasil diupdate");
                                        update("No", i, jTable1);
                                        CardLayout cl = (CardLayout) body_mhs.getLayout();
                                        cl.show(body_mhs, "menu_utama");
                                        jLabel6.setText("[" + nama1.getText() + "] masuk sebagai mahasiswa");
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(null, "Maaf, nomor telepon tidak valid", "Error", JOptionPane.ERROR_MESSAGE);
                                }
                            } else if (no_telp1.getText().equals("") && !no_hp1.getText().equals("")) {
                                if (cekAngka(no_hp1)) {
                                    db.query("update mahasiswa set nama_mahasiswa='" + nama1.getText() + "', nim='" + nim1.getText() + "',no_hp='" + no_hp1.getText() + "',no_telp='" + no_telp1.getText() + "', email='" + email1.getText() + "', alamat='" + alamat1.getText() + "', id_fakultas='" + id_fak + "', tgl_lahir='" + jDateChooser2.getDate().getYear() + "-" + (jDateChooser2.getDate().getMonth() + 1) + "-" + jDateChooser2.getDate().getDate() + "', password='" + password1.getText() + "', tahun='" + tahun1.getSelectedItem().toString() + "', nip='" + nip + "' where nim='" + temp + "'");
                                    if (db.isInsert) {
                                        JOptionPane.showMessageDialog(null, "Data berhasil diupdate");
                                        update("No", i, jTable1);
                                        CardLayout cl = (CardLayout) body_mhs.getLayout();
                                        cl.show(body_mhs, "menu_utama");
                                        jLabel6.setText("[" + nama1.getText() + "] masuk sebagai mahasiswa");
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(null, "Maaf, nomor telepon tidak valid", "Error", JOptionPane.ERROR_MESSAGE);
                                }
                            } else if (!no_hp1.getText().equals("") && !no_telp1.getText().equals("")) {
                                if (cekAngka(no_hp1) && cekAngka(no_telp1)) {
                                    db.query("update mahasiswa set nama_mahasiswa='" + nama1.getText() + "', nim='" + nim1.getText() + "',no_hp='" + no_hp1.getText() + "',no_telp='" + no_telp1.getText() + "', email='" + email1.getText() + "', alamat='" + alamat1.getText() + "', id_fakultas='" + id_fak + "', tgl_lahir='" + jDateChooser2.getDate().getYear() + "-" + (jDateChooser2.getDate().getMonth() + 1) + "-" + jDateChooser2.getDate().getDate() + "', password='" + password1.getText() + "', tahun='" + tahun1.getSelectedItem().toString() + "', nip='" + nip + "' where nim='" + temp + "'");
                                    if (db.isInsert) {
                                        JOptionPane.showMessageDialog(null, "Data berhasil diupdate");
                                        update("No", i, jTable1);
                                        CardLayout cl = (CardLayout) body_mhs.getLayout();
                                        cl.show(body_mhs, "menu_utama");
                                        jLabel6.setText("[" + nama1.getText() + "] masuk sebagai mahasiswa");
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(null, "Maaf, nomor telepon tidak valid", "Error", JOptionPane.ERROR_MESSAGE);
                                }
                            } else {
                                JOptionPane.showMessageDialog(null, "Maaf, nomor telepon harus di isi", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        } else {
                            JOptionPane.showMessageDialog(null, "Maaf, email tidak valid", "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Maaf, alamat tidak valid", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Maaf, password kurang dari 6 karakter", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Maaf, nama tidak valid", "Error", JOptionPane.ERROR_MESSAGE);
            }

        } else {
            JOptionPane.showMessageDialog(null, "Maaf, konfirmasi password tidak cocok", "Error", JOptionPane.ERROR_MESSAGE);
        }
        ulang_password1.setText("");
    }//GEN-LAST:event_jButton24ActionPerformed

    private void jButton25ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton25ActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) body_edit.getLayout();
        cl.show(body_edit, "page1");
        jButton29.setVisible(true);
    }//GEN-LAST:event_jButton25ActionPerformed
    public void setEnabled(JTextField text, JButton button) {
        if (button.isEnabled()) {
            button.setEnabled(false);
        }
        text.setEnabled(true);
    }

    public void setEnabled(JComboBox box, JButton button) {
        if (button.isEnabled()) {
            button.setEnabled(false);
        }
        box.setEnabled(true);
    }

    public void setEnabled(JTextArea box, JButton button) {
        if (button.isEnabled()) {
            button.setEnabled(false);
        }
        box.setEnabled(true);
    }

    private void namaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_namaKeyTyped
        // TODO add your handling code here:
        if (nama.getText().equals("")) {
            isi = false;
        } else {
            isi = true;
        }
        kondisi1();
    }//GEN-LAST:event_namaKeyTyped

    private void nimKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nimKeyTyped
        // TODO add your handling code here:
        if (nim.getText().equals("")) {
            isi1 = false;
        } else {
            isi1 = true;
        }
        kondisi1();
    }//GEN-LAST:event_nimKeyTyped

    private void no_hpKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_no_hpKeyTyped
        // TODO add your handling code here:
        if (no_hp.getText().equals("")) {
            isi2 = false;
        } else {
            isi2 = true;
        }
        kondisi1();
    }//GEN-LAST:event_no_hpKeyTyped

    private void no_telpKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_no_telpKeyTyped
        // TODO add your handling code here:
        if (no_telp.getText().equals("")) {
            isi3 = false;
        } else {
            isi3 = true;
        }
        kondisi1();
    }//GEN-LAST:event_no_telpKeyTyped

    private void emailKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_emailKeyTyped
        // TODO add your handling code here:
        if (email.getText().equals("")) {
            isi4 = false;
        } else {
            isi4 = true;
        }
        kondisi1();
    }//GEN-LAST:event_emailKeyTyped

    public void kondisi1() {
        if (isi && isi1 && (isi2 || isi3) && isi4 && isi5) {
            jButton5.setEnabled(true);
        } else {
            jButton5.setEnabled(false);
        }
    }
    private void alamatKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_alamatKeyTyped
        // TODO add your handling code here:
        if (alamat.getText().equals("")) {
            isi5 = false;
        } else {
            isi5 = true;
        }
        kondisi1();
    }//GEN-LAST:event_alamatKeyTyped

    private void passwordKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_passwordKeyTyped
        // TODO add your handling code here:
        if (password.getText().equals("")) {
            isi6 = false;
        } else {
            isi6 = true;
        }
        kondisi2();
    }//GEN-LAST:event_passwordKeyTyped

    private void ulang_passwordKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ulang_passwordKeyTyped
        // TODO add your handling code here:
        if (ulang_password.getText().equals("")) {
            isi7 = false;
        } else {
            isi7 = true;
        }
        kondisi2();
    }//GEN-LAST:event_ulang_passwordKeyTyped

    public void kondisi2() {
        if (isi6 && isi7 && ischeck) {
            jButton3.setEnabled(true);
        } else {
            jButton3.setEnabled(false);
        }
    }
    private void jButton27ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton27ActionPerformed
        // TODO add your handling code here:
        

        CardLayout cl = (CardLayout) body_masuk.getLayout();
        cl.show(body_masuk, "input_data");
        
    }//GEN-LAST:event_jButton27ActionPerformed

    public void update(String name1, String name2, String tabel, String data1, String data2, JTable j) {
        Database db = new Database();
        ResultSet rs = null;
        String[] nama = {name1, name2};
        DefaultTableModel tb = new DefaultTableModel(nama, 0) {
            @Override
            public boolean isCellEditable(int row, int col) {
                return false;
            }
        };
        try {
            rs = db.getResult("select * from " + tabel);
            while (rs.next()) {
                String[] data = {rs.getString(data1), rs.getString(data2)};
                tb.addRow(data);
            }
            rs.close();
            j.setModel(tb);

        } catch (SQLException f) {
            JOptionPane.showMessageDialog(null, "Terjadi kesalahan dalam database", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    private void jComboBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox2ActionPerformed
        // TODO add your handling code here:

        switch (jComboBox2.getSelectedIndex()) {
            case 0: {
                jLabel83.setText("Masukkan NIP :");
                CardLayout cl1 = (CardLayout) body_edit_data_kiri.getLayout();
                cl1.show(body_edit_data_kiri, "edit_dosen");
                isi = false;
                isi1 = false;
                jTextField11.setText("");
                jTextField12.setText("");
                jButton41.setEnabled(false);
                jTextField13.setText("");
                update("Nama Pembimbing", "NIP", "pembimbing", "nama_pembimbing", "nip", jTable4);
                break;
            }
            case 1: {
                jLabel83.setText("Masukkan ID_PA :");
                CardLayout cl1 = (CardLayout) body_edit_data_kiri.getLayout();
                cl1.show(body_edit_data_kiri, "edit_pa");
                isi = false;
                isi1 = false;
                jTextField6.setText("");
                jTextField7.setText("");
                jButton40.setEnabled(false);
                jTextField13.setText("");
                update("Judul PA", "ID PA", "pa", "judul_pa", "id_pa", jTable4);
                break;
            }
        }
    }//GEN-LAST:event_jComboBox2ActionPerformed

    private void jButton34ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton34ActionPerformed
        // TODO add your handling code here:
        edit_pa.setVisible(false);
        Database db = new Database();
        ResultSet rs = null;
        CardLayout cl = (CardLayout) body_masuk.getLayout();
        cl.show(body_masuk, "edit_data");
        CardLayout cl1 = (CardLayout) body_edit_data_kiri.getLayout();
        cl1.show(body_edit_data_kiri, "edit_dosen");
        update("Nama Pembimbing", "NIP", "pembimbing", "nama_pembimbing", "nip", jTable4);

    }//GEN-LAST:event_jButton34ActionPerformed

    private void jButton42ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton42ActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) body_masuk.getLayout();
        cl.show(body_masuk, "utama_admin");
        
        
        
    }//GEN-LAST:event_jButton42ActionPerformed

    private void jButton43ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton43ActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) body_masuk.getLayout();
        cl.show(body_masuk, "utama_admin");
        jComboBox2.setSelectedIndex(0);
        edit_dosen.setVisible(true);
        edit_pa.setVisible(false);
        jButton40.setEnabled(false);
        jButton41.setEnabled(false);
        String[] nama = {"Nama Pembimbing", "NIP"};
        DefaultTableModel tb = new DefaultTableModel(nama, 0);
        jTable4.setModel(tb);

    }//GEN-LAST:event_jButton43ActionPerformed

    private void jTextField2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField2KeyTyped
        // TODO add your handling code here:
        if (jTextField2.getText().equals("")) {
            isi = false;

        } else {
            isi = true;
        }

        if (isi && isi1) {
            jButton38.setEnabled(true);
        } else {
            jButton38.setEnabled(false);
        }
    }//GEN-LAST:event_jTextField2KeyTyped

    private void jTextField3KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField3KeyTyped
        // TODO add your handling code here:
        if (jTextField3.getText().equals("")) {
            isi1 = false;

        } else {
            isi1 = true;
        }

        if (isi && isi1) {
            jButton38.setEnabled(true);
        } else {
            jButton38.setEnabled(false);
        }
    }//GEN-LAST:event_jTextField3KeyTyped

    private void jButton38ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton38ActionPerformed
        // TODO add your handling code here:
        Database db = new Database();
        if (!cekAngka(jTextField2)) {
            int a = JOptionPane.showConfirmDialog(null, "Anda Yakin ?", "Konfirmasi", JOptionPane.OK_CANCEL_OPTION);
            if (a == 0) {
                db.query("insert into pembimbing values ('" + jTextField3.getText() + "','" + jTextField2.getText() + "')");
                if (db.isInsert) {
                    JOptionPane.showMessageDialog(null, "Input berhasil");
                    jTextField2.setText("");
                    jTextField3.setText("");
                    jButton38.setEnabled(false);
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "Maaf, nama tidak valid", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton38ActionPerformed

    private void jButton41ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton41ActionPerformed
        // TODO add your handling code here:
        if (!cekAngka(jTextField11)) {
            Database db = new Database();
            db.query("update pembimbing set nama_pembimbing='" + jTextField11.getText() + "' where nip='" + jTextField12.getText() + "'");
            if (db.isInsert) {
                JOptionPane.showMessageDialog(null, "Data berhasil diupdate");
                jTextField11.setText("");
                jTextField12.setText("");
                jTextField13.setText("");
                jButton26.setEnabled(false);
                jButton41.setEnabled(false);
                update("Nama Pembimbing", "NIP", "pembimbing", "nama_pembimbing", "nip", jTable4);

            }
        } else {
            JOptionPane.showMessageDialog(null, "Maaf, nama tidak valid", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton41ActionPerformed

    private void jTextField13KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField13KeyTyped
        // TODO add your handling code here:
        if (jTextField13.getText().equals("")) {
            isi = false;
        } else {
            isi = true;
        }

        if (isi) {
            jButton26.setEnabled(true);
        } else {
            jButton26.setEnabled(false);
        }
    }//GEN-LAST:event_jTextField13KeyTyped

    private void jButton26ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton26ActionPerformed
        // TODO add your handling code here:
        Database db = new Database();
        ResultSet rs = null;
        boolean ketemu = false;

        switch (jComboBox2.getSelectedIndex()) {
            case 0: {

                try {
                    rs = db.getResult("select * from pembimbing where nip='" + jTextField13.getText() + "'");
                    while (rs.next()) {
                        if (jTextField13.getText().equalsIgnoreCase(rs.getString("nip"))) {
                            ketemu = true;
                            jTextField11.setText(rs.getString("nama_pembimbing"));
                            jTextField12.setText(rs.getString("nip"));
                        } else {
                            ketemu = false;
                        }
                    }
                    if (!ketemu) {
                        jTextField6.setText("");
                        jTextField7.setText("");
                        jButton40.setEnabled(false);
                        JOptionPane.showMessageDialog(null, "Maaf, NIP tidak terdaftar", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    rs.close();

                } catch (SQLException f) {
                    JOptionPane.showMessageDialog(null, "" + f.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }

                if (!jTextField11.getText().equals("")) {
                    jButton41.setEnabled(true);
                    jButton40.setEnabled(false);
                } else {
                    jButton41.setEnabled(false);
                    jButton40.setEnabled(false);
                }
                break;
            }
            case 1: {

                try {
                    rs = db.getResult("select * from pa where id_pa='" + jTextField13.getText() + "'");
                    while (rs.next()) {
                        if (rs.getString("id_pa").equalsIgnoreCase(jTextField13.getText())) {
                            ketemu = true;
                            jTextField6.setText(rs.getString("judul_pa"));
                            jTextField7.setText(rs.getString("id_pa"));
                        } else {
                            ketemu = false;
                        }

                    }
                    if (!ketemu) {
                        jTextField6.setText("");
                        jTextField7.setText("");
                        jButton40.setEnabled(false);
                        JOptionPane.showMessageDialog(null, "Maaf, ID PA tidak terdaftar", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    rs.close();
                } catch (SQLException f) {
                    JOptionPane.showMessageDialog(null, "" + f.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
                if (!jTextField6.getText().equals("")) {
                    jButton40.setEnabled(true);
                    jButton41.setEnabled(false);
                } else {
                    jButton41.setEnabled(false);
                    jButton40.setEnabled(false);
                }

                break;
            }

        }
    }//GEN-LAST:event_jButton26ActionPerformed

    private void jButton29ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton29ActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) body_mhs.getLayout();
        cl.show(body_mhs, "menu_utama");
    }//GEN-LAST:event_jButton29ActionPerformed

    private void jButton28ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton28ActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) body_masuk.getLayout();
        cl.show(body_masuk, "download_data");
        this.isDown = true;
        update("ID PA", jTable3);
        System.out.println(isAdmin);


    }//GEN-LAST:event_jButton28ActionPerformed

    private void jTextField11KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField11KeyTyped
        // TODO add your handling code here:
        if (jTextField11.getText().equals("")) {
            jButton41.setEnabled(false);
        } else {
            jButton41.setEnabled(true);
        }
    }//GEN-LAST:event_jTextField11KeyTyped

    private void jTextField6KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField6KeyTyped
        // TODO add your handling code here:
        if (jTextField6.getText().equals("")) {
            jButton40.setEnabled(false);
        } else {
            jButton40.setEnabled(true);
        }
    }//GEN-LAST:event_jTextField6KeyTyped

    private void jButton40ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton40ActionPerformed
        // TODO add your handling code here:
        if (!cekAngka(jTextField16)) {
            Database db = new Database();
            db.query("update pa set judul_pa='" + jTextField6.getText() + "' where id_pa='" + jTextField7.getText() + "'");
            if (db.isInsert) {
                JOptionPane.showMessageDialog(null, "Data berhasil diupdate");
                jTextField6.setText("");
                jTextField7.setText("");
                jTextField13.setText("");
                jButton26.setEnabled(false);
                jButton40.setEnabled(false);
                update("Judul PA", "ID PA", "pa", "judul_pa", "id_pa", jTable4);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Maaf, judul PA tidak valid", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton40ActionPerformed

    private void jTextArea2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextArea2KeyTyped
        // TODO add your handling code here:
        if (jTextArea2.getText().equals(""))
        {
            isi = false;
        }
        else
        {
            isi = true;
        }
        
        if (isi && isi1)
        {
            jButton11.setEnabled(true);
            
            
        }
        else
        {
            jButton11.setEnabled(false);
        }
    }//GEN-LAST:event_jTextArea2KeyTyped

    private void jButton30ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton30ActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) body_masuk.getLayout();
        cl.show(body_masuk, "utama_admin");
        jButton31.setEnabled(false);
        jTextField14.setText("");
        jComboBox3.setSelectedIndex(0);
    }//GEN-LAST:event_jButton30ActionPerformed

    private void jTextField14KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField14KeyTyped
        // TODO add your handling code here:
        if (jTextField14.getText().equals("")) {
            isi = false;
        } else {
            isi = true;
        }

        if (isi) {
            jButton31.setEnabled(true);
        } else {
            jButton31.setEnabled(false);
        }
    }//GEN-LAST:event_jTextField14KeyTyped

    private void jComboBox5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox5ActionPerformed
        // TODO add your handling code here:
        switch (jComboBox5.getSelectedIndex()) {
            case 0: {
                cariPA("order", "nim", jTable2);
                break;
            }
            case 1: {
                cariPA("order", "judul_pa", jTable2);
                break;
            }
        }
    }//GEN-LAST:event_jComboBox5ActionPerformed

    private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1ActionPerformed
        // TODO add your handling code here:
        if (jCheckBox1.isSelected()) {
            ischeck = true;

        } else {
            ischeck = false;
        }
        kondisi2();
    }//GEN-LAST:event_jCheckBox1ActionPerformed

    private void jComboBox3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox3ActionPerformed
        // TODO add your handling code here:`
        switch (jComboBox3.getSelectedIndex()) {
            case 0: {
                cariPA("order", "nim");
                break;
            }
            case 1: {
                cariPA("order", "id_pa");
                break;
            }
        }


    }//GEN-LAST:event_jComboBox3ActionPerformed

    private void jButton31ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton31ActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) body_masuk.getLayout();
        cl.show(body_masuk, "detail");

    }//GEN-LAST:event_jButton31ActionPerformed

    public void display(JTable j, int column) {
        Database db = new Database();
        ResultSet rs = null;
        int row = j.getSelectedRow();


        try {
            rs = db.getResult("select * from mahasiswa join pa using(nim) join pembimbing using(nip) where nim='" + j.getValueAt(row, column) + "'");
            while (rs.next()) {
                jLabel91.setText(rs.getString("judul_pa"));
                jTextField16.setText(rs.getString("nama_mahasiswa"));
                jTextField17.setText(rs.getString("nim"));
                jTextField15.setText(rs.getString("nama_pembimbing"));
                jTextArea1.setText(rs.getString("abstraksi"));

            }
        } catch (SQLException f) {
            JOptionPane.showMessageDialog(null, "" + f.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
        if (!isAdmin) {
            CardLayout cl = (CardLayout) Body.getLayout();
            cl.show(Body, "detail");
            jLabel8.setVisible(false);
            jLabel9.setVisible(false);
            display(jTable1, 2);
        }
    }//GEN-LAST:event_jTable1MouseClicked

    private void jButton32ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton32ActionPerformed
        // TODO add your handling code here:
        if (!isAdmin) {
            if (!isCari) {
                CardLayout cl = (CardLayout) Body.getLayout();
                cl.show(Body, "depan");
                jLabel8.setVisible(true);
                jLabel9.setVisible(true);
            } else {
                CardLayout cl = (CardLayout) Body.getLayout();
                cl.show(Body, "cari");
            }
        }




    }//GEN-LAST:event_jButton32ActionPerformed

    private void jTable2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable2MouseClicked
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) Body.getLayout();
        cl.show(Body, "detail");
        display(jTable2, 1);
    }//GEN-LAST:event_jTable2MouseClicked

    private void jTable4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable4MouseClicked
        // TODO add your handling code here:
        int row = jTable4.getSelectedRow();
        switch (jComboBox2.getSelectedIndex()) {
            case 0: {
                jTextField11.setText(jTable4.getValueAt(row, 0).toString());
                jTextField12.setText(jTable4.getValueAt(row, 1).toString());
                jButton41.setEnabled(true);
                break;
            }
            case 1: {
                jTextField6.setText(jTable4.getValueAt(row, 0).toString());
                jTextField7.setText(jTable4.getValueAt(row, 1).toString());
                jButton40.setEnabled(true);
                break;
            }
        }



    }//GEN-LAST:event_jTable4MouseClicked

    private void jTable3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable3MouseClicked
        // TODO add your handling code here:
        jTextField14.setText(jTable3.getValueAt(jTable3.getSelectedRow(), 0).toString());
        jButton31.setEnabled(true);
        Database db = new Database();
        ResultSet rs = null;
        int row = jTable3.getSelectedRow();
        try {
            rs = db.getResult("select * from mahasiswa join pa using(nim) join pembimbing using(nip) where nim='" + jTable3.getValueAt(row, 2) + "'");
            while (rs.next()) {
                jLabel106.setText(rs.getString("judul_pa"));
                jTextField19.setText(rs.getString("nama_mahasiswa"));
                jTextField20.setText(rs.getString("nim"));
                jTextField18.setText(rs.getString("nama_pembimbing"));
                jTextArea3.setText(rs.getString("abstraksi"));

            }
        } catch (SQLException f) {
            JOptionPane.showMessageDialog(null, "" + f.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_jTable3MouseClicked

    public boolean isUpload(String nim)
    {
        boolean isAda = true;
        Database db = new Database();
        ResultSet rs = null;
        try
        {
            rs = db.getResult("select nim from pa where nim='"+nim+"'");
            while (rs.next())
            {
                if (rs.getString("nim").equals(nim))
                {
                    isAda = true;
                }
                else
                {
                    isAda = false;
                }
            }
            rs.close();
        }
        catch (SQLException f)
        {
            isAda = false;
        }
        return isAda;
    }
    
    private void jButton33ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton33ActionPerformed
        // TODO add your handling code here:
        Database db = new Database();
        int i=0;
        int a = JOptionPane.showConfirmDialog(null, "Anda Yakin ?", "Konfirmasi", JOptionPane.OK_CANCEL_OPTION);
        if (a == 0) {
            
            try {
                if (!ada)
                {
                    String ID_PA=generateIdPA();
                    String query="insert into pa values('"+ID_PA+"','"+jTextField21.getText()+"','"+ jTextArea2.getText()+"','"+temp+"','"+URL+"')";
                    db.query(query);
                    if (db.isInsert)
                    {
                        try {
                            upload(URLAsal, URL);
                        } catch (IOException ex) {
                            Logger.getLogger(Screen.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        JOptionPane.showMessageDialog(null, "Upload berhasil");
                        CardLayout cl = (CardLayout) body_mhs.getLayout();
                        cl.show(body_mhs, "menu_utama");
                        update("No", i, jTable1);
                    }
                }
                else
                {
                    db.query("update pa set judul_pa='"+jTextField21.getText()+"', abstraksi='"+jTextArea2.getText()+"', url='"+URL+"' where nim='"+temp+"'");
                    if (db.isInsert)
                    {
                        try {
                            upload(URLAsal, URL);
                        } catch (IOException ex) {
                            Logger.getLogger(Screen.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        JOptionPane.showMessageDialog(null, "Upload berhasil");
                        CardLayout cl = (CardLayout) body_mhs.getLayout();
                        cl.show(body_mhs, "menu_utama");
                        update("No", i, jTable1);
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(Screen.class.getName()).log(Level.SEVERE, null, ex);
            }
            jTextField21.setText("");
            jButton11.setEnabled(false);
            jTextArea2.setText("");
            jLabel107.setText("?");
            jCheckBox2.setSelected(false);
            jButton33.setEnabled(false);
        }
    }//GEN-LAST:event_jButton33ActionPerformed

    private void jButton36ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton36ActionPerformed
        // TODO add your handling code here:
        Database db = new Database();
        String url = "";
        String query = "select url from pa where nim='"+jTextField20.getText()+"'";
        try
        {
            ResultSet rs = db.getResult(query);
            while (rs.next())
            {
                url = rs.getString("url");
            }
            rs.close();
        }
        catch (SQLException f)
        {
            JOptionPane.showMessageDialog(null, "Terjadi kesalahan dalam database\n"+f.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
        
        String URLTarget = "C:/Users/FANSKY/Downloads/"+jTextField20.getText()+".pdf";
        try
        {
            upload(url, URLTarget);
            JOptionPane.showMessageDialog(null, "Download berhasil");
            CardLayout cl = (CardLayout)body_masuk.getLayout();
            cl.show(body_masuk,"download_data");
        }
        catch (Throwable f)
        {
            JOptionPane.showMessageDialog(null, "Download gagal", "Error", JOptionPane.ERROR_MESSAGE);
            f.printStackTrace();
            
        }
        
        
        
    }//GEN-LAST:event_jButton36ActionPerformed

    private void jButton37ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton37ActionPerformed
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) body_masuk.getLayout();
        cl.show(body_masuk, "download_data");

    }//GEN-LAST:event_jButton37ActionPerformed

    private void jTextField21KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField21KeyTyped
        // TODO add your handling code here:
        if (jTextField21.getText().equals(""))
        {
            isi1 = false;
        }
        else
        {
            isi1 = true;
        }
        
        if (isi && isi1)
        {
            jButton11.setEnabled(true);
            
           
        }
        else
        {
            jButton11.setEnabled(false);
        }
        
        
    }//GEN-LAST:event_jTextField21KeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(final String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            UIManager.setLookAndFeel(new WindowsLookAndFeel());
        } catch (Throwable f) {
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Screen scr = new Screen();
                scr.setTitle("Aplikasi Pengelolaan Proyek Akhir");
                scr.setVisible(true);
                scr.setResizable(false);

                scr.setLocationRelativeTo(null);

            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem About;
    private javax.swing.JPanel Admin;
    private javax.swing.JPanel Body;
    private javax.swing.JPanel Login;
    private javax.swing.JPanel Menu_Upload;
    private javax.swing.JPanel Menu_cari;
    private javax.swing.JPanel Menu_mhs;
    private javax.swing.JTextArea alamat;
    private javax.swing.JTextArea alamat1;
    private javax.swing.JPanel awal;
    private javax.swing.JPanel back;
    private javax.swing.JPanel background;
    private javax.swing.JPanel body_admin;
    private javax.swing.JPanel body_cari;
    private javax.swing.JPanel body_depan;
    private javax.swing.JPanel body_detail;
    private javax.swing.JPanel body_detail1;
    private javax.swing.JPanel body_download;
    private javax.swing.JPanel body_edit;
    private javax.swing.JPanel body_edit_data;
    private javax.swing.JPanel body_edit_data_kanan;
    private javax.swing.JPanel body_edit_data_kiri;
    private javax.swing.JPanel body_input;
    private javax.swing.JPanel body_login;
    private javax.swing.JPanel body_lupa;
    private javax.swing.JPanel body_masuk;
    private javax.swing.JPanel body_mhs;
    private javax.swing.JPanel body_pilih;
    private javax.swing.JPanel body_registrasi;
    private javax.swing.JPanel body_upload;
    private javax.swing.JPanel cari_kanan;
    private javax.swing.JPanel cari_kiri;
    private javax.swing.JPanel display_detail;
    private javax.swing.JPanel display_detail1;
    private javax.swing.JPanel download_data;
    private javax.swing.JMenu edit;
    private javax.swing.JPanel edit_data;
    private javax.swing.JPanel edit_dosen;
    private javax.swing.JPanel edit_pa;
    private javax.swing.JTextField email;
    private javax.swing.JTextField email1;
    private javax.swing.JComboBox fakultas;
    private javax.swing.JComboBox fakultas1;
    private javax.swing.JMenu file;
    private javax.swing.JLabel footer;
    private javax.swing.JPanel footer_detail;
    private javax.swing.JPanel footer_detail1;
    private javax.swing.JPanel footer_edit;
    private javax.swing.JPanel footer_input;
    private javax.swing.JPanel header;
    private javax.swing.JPanel header_detail;
    private javax.swing.JPanel header_detail1;
    private javax.swing.JPanel header_download;
    private javax.swing.JPanel header_edit;
    private javax.swing.JPanel header_input;
    private javax.swing.JPanel header_mhs;
    private javax.swing.JMenu help;
    private javax.swing.JPanel home;
    private javax.swing.JPanel input_data;
    private javax.swing.JPanel input_dosen;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton14;
    private javax.swing.JButton jButton15;
    private javax.swing.JButton jButton16;
    private javax.swing.JButton jButton17;
    private javax.swing.JButton jButton18;
    private javax.swing.JButton jButton19;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton20;
    private javax.swing.JButton jButton21;
    private javax.swing.JButton jButton22;
    private javax.swing.JButton jButton23;
    private javax.swing.JButton jButton24;
    private javax.swing.JButton jButton25;
    private javax.swing.JButton jButton26;
    private javax.swing.JButton jButton27;
    private javax.swing.JButton jButton28;
    private javax.swing.JButton jButton29;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton30;
    private javax.swing.JButton jButton31;
    private javax.swing.JButton jButton32;
    private javax.swing.JButton jButton33;
    private javax.swing.JButton jButton34;
    private javax.swing.JButton jButton36;
    private javax.swing.JButton jButton37;
    private javax.swing.JButton jButton38;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton40;
    private javax.swing.JButton jButton41;
    private javax.swing.JButton jButton42;
    private javax.swing.JButton jButton43;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JComboBox jComboBox3;
    private javax.swing.JComboBox jComboBox5;
    private javax.swing.JComboBox jComboBox6;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private com.toedter.calendar.JDateChooser jDateChooser2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel100;
    private javax.swing.JLabel jLabel101;
    private javax.swing.JLabel jLabel102;
    private javax.swing.JLabel jLabel103;
    private javax.swing.JLabel jLabel104;
    private javax.swing.JLabel jLabel105;
    private javax.swing.JLabel jLabel106;
    private javax.swing.JLabel jLabel107;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel73;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel77;
    private javax.swing.JLabel jLabel78;
    private javax.swing.JLabel jLabel79;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel80;
    private javax.swing.JLabel jLabel81;
    private javax.swing.JLabel jLabel82;
    private javax.swing.JLabel jLabel83;
    private javax.swing.JLabel jLabel84;
    private javax.swing.JLabel jLabel85;
    private javax.swing.JLabel jLabel86;
    private javax.swing.JLabel jLabel87;
    private javax.swing.JLabel jLabel88;
    private javax.swing.JLabel jLabel89;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabel90;
    private javax.swing.JLabel jLabel91;
    private javax.swing.JLabel jLabel92;
    private javax.swing.JLabel jLabel93;
    private javax.swing.JLabel jLabel94;
    private javax.swing.JLabel jLabel95;
    private javax.swing.JLabel jLabel96;
    private javax.swing.JLabel jLabel98;
    private javax.swing.JLabel jLabel99;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPasswordField jPasswordField1;
    private javax.swing.JPasswordField jPasswordField4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    private javax.swing.JTable jTable4;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextArea jTextArea2;
    private javax.swing.JTextArea jTextArea3;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField10;
    private javax.swing.JTextField jTextField11;
    private javax.swing.JTextField jTextField12;
    private javax.swing.JTextField jTextField13;
    private javax.swing.JTextField jTextField14;
    private javax.swing.JTextField jTextField15;
    private javax.swing.JTextField jTextField16;
    private javax.swing.JTextField jTextField17;
    private javax.swing.JTextField jTextField18;
    private javax.swing.JTextField jTextField19;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField20;
    private javax.swing.JTextField jTextField21;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JTextField jTextField9;
    private com.toedter.calendar.JYearChooser jYearChooser1;
    private javax.swing.JPanel kanan;
    private javax.swing.JPanel kanan_body;
    private javax.swing.JPanel kiri;
    private javax.swing.JPanel kiri_body;
    private javax.swing.JPanel login_admin;
    private javax.swing.JPanel lupa_pass;
    private javax.swing.JPanel masuk_admin;
    private javax.swing.JPanel menu_edit_akun;
    private javax.swing.JPanel menu_utama;
    private javax.swing.JPanel menu_utama_admin;
    private javax.swing.JTextField nama;
    private javax.swing.JTextField nama1;
    private javax.swing.JTextField nim;
    private javax.swing.JTextField nim1;
    private javax.swing.JTextField no_hp;
    private javax.swing.JTextField no_hp1;
    private javax.swing.JTextField no_telp;
    private javax.swing.JTextField no_telp1;
    private javax.swing.JPanel page1;
    private javax.swing.JPanel page2;
    private javax.swing.JPanel page3;
    private javax.swing.JPanel page4;
    private javax.swing.JPasswordField password;
    private javax.swing.JPasswordField password1;
    private javax.swing.JComboBox pembimbing;
    private javax.swing.JComboBox pembimbing1;
    private javax.swing.JPanel pilih_user;
    private javax.swing.JPanel registrasi;
    private javax.swing.JComboBox tahun;
    private javax.swing.JComboBox tahun1;
    private javax.swing.JPanel title_cari;
    private javax.swing.JPanel title_login;
    private javax.swing.JPanel title_lupa;
    private javax.swing.JPanel title_masuk;
    private javax.swing.JPanel title_pilih;
    private javax.swing.JPanel title_registrasi;
    private javax.swing.JPasswordField ulang_password;
    private javax.swing.JPasswordField ulang_password1;
    private javax.swing.JPanel utama;
    // End of variables declaration//GEN-END:variables
}
