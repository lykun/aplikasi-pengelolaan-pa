/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package PA;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author FANSKY
 */
public class Mahasiswa implements Orang{
    private String nim;
    private String nama;
    private int tahun;
    private String password;
    private String email;
    private ArrayList<Pembimbing> pembimbingTetap = new ArrayList<>();
    
    public Mahasiswa(String nama, String nim, int tahun, String password, String email)
    {
        this.nama = nama;
        this.nim = nim;
        this.tahun = tahun;
        this.password = password;
        this.email = email;
    }

    @Override
    public String getId() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    @Override
    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getTahun() {
        return tahun;
    }

    public void setTahun(int tahun) {
        this.tahun = tahun;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public void addPembimbing(Pembimbing dsn)
    {
        try
        {
            if (pembimbingTetap.isEmpty())
            {
                pembimbingTetap.add(dsn);
                JOptionPane.showMessageDialog(null, "Dosen berhasil diset", "Konfirmasi", JOptionPane.OK_CANCEL_OPTION);
            }
            else
            {
                JOptionPane.showMessageDialog(null, "Dosen sudah diset", "Konfirmasi", JOptionPane.ERROR_MESSAGE);
            }
        }
        catch (Throwable f)
        {
            JOptionPane.showMessageDialog(null, "Gagal diset", "Konfirmasi", JOptionPane.ERROR_MESSAGE);
        }
                
    }

    public ArrayList<Pembimbing> getPembimbingTetap() {
        return pembimbingTetap;
    }
    
   public Pembimbing searchingPembimbingbyId(String id)
    {
        Pembimbing cari = null;
        for (Pembimbing p : getPembimbingTetap())
        {
            if (id.equals(p.getId()))
            {
                cari = p;
                break;
            }
        }
        return cari;
    }
   
    
    
    
    
}
